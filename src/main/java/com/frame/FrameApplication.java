package com.frame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.request.RequestContextListener;

@SpringBootApplication
public class FrameApplication {

	/**
	 * 解决上下文的request容器为null的问题
	 * RequestContextHolder.getRequestAttributes()
	 * @return
	 */
	@Bean
	public RequestContextListener requestContextListener(){
	    return new RequestContextListener();
	}

	
	public static void main(String[] args) {
		SpringApplication.run(FrameApplication.class, args);
	}

}
