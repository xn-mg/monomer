package com.frame.project.domain.system.menu;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.frame.framework.web.domain.BaseEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：Menu
 * @describe ：菜单权限表
 */
@Entity
@Table(name = "sys_menu")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_menu",comment = "菜单权限表")
public class Menu extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** 菜单名称 */
    @NotBlank(message = "菜单名称不能为空")
    @Size(min = 0, max = 50, message = "菜单名称长度不能超过50个字符")
    @Column(name = "menu_name")
    private String menuName;

    /** 父菜单ID */
    @Column(name = "parent_id")
    private Long parentId;

    /** 父菜单名称 */
    @Transient //不在数据库中做映射
    private String parentName;
    
    /** 菜单URL */
    @Size(min = 0, max = 200, message = "请求地址不能超过200个字符")
    private String url;

    /** 打开方式：menuItem页签 menuBlank新窗口 */
    private String target;

    /** 类型:0目录,1菜单,2按钮 */
    @NotBlank(message = "菜单类型不能为空")
    @Column(name = "menu_type")
    private String menuType;

    /** 菜单状态:0显示,1隐藏 */
    private String visible;

    /** 是否刷新（0刷新 1不刷新） */
    private String isRefresh;
    
    /** 权限字符串 */
    @Size(min = 0, max = 100, message = "权限标识长度不能超过100个字符")
    private String perms;

    /** 菜单图标 */
    private String icon;

    /** 子菜单 */
    @Transient //不在数据库中做映射
    private List<Menu> children = new ArrayList<Menu>();

}
