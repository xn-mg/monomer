package com.frame.project.domain.system.notice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.frame.framework.web.domain.BaseEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：Notice
 * @describe ：通知公告表
 */
@Entity
@Table(name = "sys_notice")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_notice",comment = "通知公告表")
public class Notice extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** 公告标题 */
    @NotBlank(message = "公告标题不能为空")
    @Size(min = 0, max = 50, message = "公告标题不能超过50个字符")
    @Column(name = "notice_title")
    private String noticeTitle;

    /** 公告类型（1通知 2公告） */
    @Column(name = "notice_type")
    private String noticeType;

    /** 公告内容 */
    @Column(name = "notice_content",columnDefinition = "longtext", nullable = true)
    private String noticeContent;

    /** 公告状态（0正常 1关闭） */
    private String status;

}
