package com.frame.project.domain.system.role;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.frame.project.domain.system.dept.Dept;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：RoleDept
 * @describe ：角色和部门关联
 */
@Entity
@Table(name = "sys_role_dept")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_role_dept",comment = "角色和部门关联表")
public class RoleDept implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 部门 */
    @ManyToOne
	@JoinColumn(name="dept_id")
	private Dept dept;
	
    /** 角色 */
	@ManyToOne
	@JoinColumn(name="role_id")
	private Role role;
}
