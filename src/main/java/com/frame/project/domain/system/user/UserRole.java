package com.frame.project.domain.system.user;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.frame.project.domain.system.role.Role;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：UserRole
 * @describe ：用户和角色关联
 */
@Entity
@Table(name = "sys_user_role")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_user_role",comment = "用户和角色关联表")
public class UserRole implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 用户 */
    @ManyToOne
	@JoinColumn(name="user_id")
	private User user;
    
    /** 角色 */
    @ManyToOne
	@JoinColumn(name="role_id")
	private Role role;

}
