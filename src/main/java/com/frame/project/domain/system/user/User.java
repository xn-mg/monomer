package com.frame.project.domain.system.user;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import com.frame.common.utils.StringUtils;
import com.frame.framework.aspectj.lang.annotation.Excel;
import com.frame.framework.aspectj.lang.annotation.Excel.Type;
import com.frame.framework.web.domain.BaseEntity;
import com.frame.project.domain.system.dept.Dept;
import com.frame.project.domain.system.role.Role;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：User
 * @describe ：用户对象
 */
@Entity
@Table(name = "sys_user")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_user",comment = "用户信息表")
public class User extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    
    /** 部门ID */
    @Excel(name = "部门编号", type = Type.IMPORT)
    @Column(name = "dept_id")
    private Long deptId;
    
    /** 部门 */
    @ManyToOne
	@JoinColumn(name="dept")
	private Dept dept;
    
    /** 部门父ID */
    @Transient    //不在数据库中做映射
    private Long parentId;

    /** 角色ID */
    @Transient   //不在数据库中做映射
    private Long roleId;
    
    /** 登录名称 */
    @Excel(name = "登录名称")
    @NotBlank(message = "登录账号不能为空")
    @Size(min = 0, max = 30, message = "登录账号长度不能超过30个字符")
    @Column(name = "login_name")
    private String loginName;

    /** 用户名称 */
    @Excel(name = "用户名称")
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    @Column(name = "user_name")
    private String userName;

    /** 用户邮箱 */
    @Excel(name = "用户邮箱")
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    private String email;

    /** 手机号码 */
    @Excel(name = "手机号码")
    @Column(name = "phone_number")
    private String phonenumber;

    /** 用户性别 */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 密码 */
    private String password;

    /** 盐加密 */
    private String salt;
    
    /** 密码最后更新时间 */
    @Column(name = "pwd_update_date")
    private Date pwdUpdateDate;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 最后登陆IP */
    @Excel(name = "最后登陆IP", type = Type.EXPORT)
    @Column(name = "login_ip")
    private String loginIp;

    /** 最后登陆时间 */
    @Excel(name = "最后登陆时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
    @Column(name = "login_date")
    private Date loginDate;

    @Transient //不在数据库中做映射
    private List<Role> roles;
    
    /** 角色组 */
    @Transient //不在数据库中做映射
    private Long[] roleIds;

    /** 岗位组 */
    @Transient //不在数据库中做映射
    private Long[] postIds;
	
	/** 请求参数 */
	@Transient //不在数据库中做映射
    private Map<String, Object> params;
	
    public boolean isAdmin()
    {
        return isAdmin(this.loginName);
    }

    public static boolean isAdmin(String loginName)
    {
        return StringUtils.isNotBlank(loginName) && "admin".equals(loginName);
    }

    /**
     * 生成随机盐
     */
    public void randomSalt()
    {
        // 一个Byte占两个字节，此处生成的3字节，字符串长度为6
        SecureRandomNumberGenerator secureRandom = new SecureRandomNumberGenerator();
        String hex = secureRandom.nextBytes(3).toHex();
        setSalt(hex);
    }


}
