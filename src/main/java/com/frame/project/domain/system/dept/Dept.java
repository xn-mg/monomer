package com.frame.project.domain.system.dept;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.alibaba.fastjson.annotation.JSONField;
import com.frame.framework.web.domain.BaseEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：Dept
 * @describe ：部门表
 */
@Entity
@Table(name = "sys_dept")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_dept",comment = "部门表")
public class Dept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @ManyToOne
	@JoinColumn(name = "sup_dept")
	@JSONField(serialize = false)
	private Dept supDept;

    /** 父部门ID */
    @Column(name = "parent_id")
    private Long parentId;
    
    /** 祖级列表 */
    private String ancestors;

    /** 部门名称 */
    @NotBlank(message = "部门名称不能为空")
    @Size(min = 0, max = 30, message = "部门名称长度不能超过30个字符")
    @Column(name = "dept_name")
    private String deptName;

    /** 负责人 */
    private String leader;

    /** 联系电话 */
    @Size(min = 0, max = 11, message = "联系电话长度不能超过11个字符")
    private String phone;

    /** 邮箱 */
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    private String email;

    /** 部门状态:0正常,1停用 */
    private String status;

}
