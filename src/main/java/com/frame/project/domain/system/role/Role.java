package com.frame.project.domain.system.role;

import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import com.frame.framework.aspectj.lang.annotation.Excel;
import com.frame.framework.web.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：Role
 * @describe ：角色表
 */
@Entity
@Table(name = "sys_role")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_role",comment = "角色表")
public class Role extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    
    /** 角色名称 */
    @Excel(name = "角色名称")
    @NotBlank(message = "角色名称不能为空")
    @Size(min = 0, max = 30, message = "角色名称长度不能超过30个字符")
    @Column(name = "role_name")
    private String roleName;

    /** 角色权限 */
    @Excel(name = "角色权限")
    @NotBlank(message = "权限字符不能为空")
    @Size(min = 0, max = 100, message = "权限字符长度不能超过100个字符")
    @Column(name = "role_key")
    private String roleKey;

    /** 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限） */
    @Excel(name = "数据范围", readConverterExp = "1=所有数据权限,2=自定义数据权限,3=本部门数据权限,4=本部门及以下数据权限")
    @Column(name = "data_scope")
    private String dataScope;

    /** 角色状态（0正常 1停用） */
    @Excel(name = "角色状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 用户是否存在此角色标识 默认不存在 */
    private boolean flag = false;
    
    /** 菜单组 */
    @Transient //不在数据库中做映射
    private Long[] menuIds;

    /** 部门组 */
    @Transient //不在数据库中做映射
    private Long[] deptIds;
    
    @Transient //不在数据库中做映射
    private Long userId;
	
	/** 请求参数 */
	@Transient //不在数据库中做映射
	private Map<String, Object> params;   
    
    public boolean isAdmin()
    {
        return isAdmin(super.getId());
    }

    public static boolean isAdmin(Long roleId)
    {
        return roleId != null && 1L == roleId;
    }
}
