package com.frame.project.domain.system.dict;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.frame.common.constant.UserConstants;
import com.frame.framework.aspectj.lang.annotation.Excel;
import com.frame.framework.aspectj.lang.annotation.Excel.ColumnType;
import com.frame.framework.web.domain.BaseEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：DictData
 * @describe ：字典数据表
 */
@Entity
@Table(name = "sys_dict_data")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_dict_data",comment = "字典数据表")
public class DictData extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** 字典排序 */
    @Excel(name = "字典排序", cellType = ColumnType.NUMERIC)
    @Column(name = "dict_sort")
    private Integer dictSort;

    /** 字典标签 */
    @Excel(name = "字典标签")
    @NotBlank(message = "字典标签不能为空")
    @Size(min = 0, max = 100, message = "字典标签长度不能超过100个字符")
    @Column(name = "dict_label")
    private String dictLabel;

    /** 字典键值 */
    @Excel(name = "字典键值")
    @NotBlank(message = "字典键值不能为空")
    @Size(min = 0, max = 100, message = "字典键值长度不能超过100个字符")
    @Column(name = "dict_value")
    private String dictValue;

    /** 字典类型 */
    @Excel(name = "字典类型")
    @NotBlank(message = "字典类型不能为空")
    @Size(min = 0, max = 100, message = "字典类型长度不能超过100个字符")
    @Column(name = "dict_type")
    private String dictType;

    /** 样式属性（其他样式扩展） */
    @Excel(name = "字典样式")
    @Size(min = 0, max = 100, message = "样式属性长度不能超过100个字符")
    @Column(name = "css_class")
    private String cssClass;

    /** 表格字典样式 */
    @Column(name = "list_class")
    private String listClass;

    /** 是否默认（Y是 N否） */
    @Excel(name = "是否默认", readConverterExp = "Y=是,N=否")
    @Column(name = "is_default")
    private String isDefault;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    
    public boolean getDefault()
    {
        return UserConstants.YES.equals(this.isDefault) ? true : false;
    }



}
