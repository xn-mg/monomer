package com.frame.project.domain.system.role;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.frame.project.domain.system.menu.Menu;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：RoleMenu
 * @describe ：角色和菜单关联
 */
@Entity
@Table(name = "sys_role_menu")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_role_menu",comment = "角色和菜单关联表")
public class RoleMenu implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 菜单 */
    @ManyToOne
	@JoinColumn(name="menu_id")
	private Menu menu;
	
    /** 角色 */
	@ManyToOne
	@JoinColumn(name="role_id")
	private Role role;
}
