package com.frame.project.domain.system.dict;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.frame.framework.aspectj.lang.annotation.Excel;
import com.frame.framework.web.domain.BaseEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：DictType
 * @describe ：字典类型表
 */
@Entity
@Table(name = "sys_dict_type")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_dict_type",comment = "字典类型表")
public class DictType extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** 字典名称 */
    @Excel(name = "字典名称")
    @NotBlank(message = "字典名称不能为空")
    @Size(min = 0, max = 100, message = "字典类型名称长度不能超过100个字符")
    @Column(name = "dict_name")
    private String dictName;

    /** 字典类型 */
    @Excel(name = "字典类型")
    @NotBlank(message = "字典类型不能为空")
    @Size(min = 0, max = 100, message = "字典类型类型长度不能超过100个字符")
    @Column(name = "dict_type")
    private String dictTypes;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;
    
}
