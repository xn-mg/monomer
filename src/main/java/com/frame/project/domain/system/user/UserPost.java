package com.frame.project.domain.system.user;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.frame.project.domain.system.post.Post;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：UserPost
 * @describe ：用户和岗位关联
 */
@Entity
@Table(name = "sys_user_post")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_user_post",comment = "用户和岗位关联表")
public class UserPost implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 用户 */
    @ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
    /** 岗位 */
	@ManyToOne
	@JoinColumn(name="post_id")
	private Post post;

}
