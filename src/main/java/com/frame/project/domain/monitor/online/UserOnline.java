package com.frame.project.domain.monitor.online;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.frame.common.enumutil.OnlineEnum;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：UserOnline
 * @describe ：当前在线会话
 */
@Entity
@Table(name = "sys_user_online")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_user_online",comment = "在线用户记录")
public class UserOnline implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    
    /** 用户会话id */
    @Column(name = "session_id")
    private String sessionId;
    
    /** 部门名称 */
    @Column(name = "dept_name")
    private String deptName;

    /** 登录名称 */
    @Column(name = "login_name")
    private String loginName;

    /** 登录IP地址 */
    private String ipaddr;

    /** 登录地址 */
    @Column(name = "login_location")
    private String loginLocation;

    /** 浏览器类型 */
    private String browser;

    /** 操作系统 */
    private String os;

    /** session创建时间 */
    @Column(name = "start_time_stamp")
    private Date startTimestamp;

    /** session最后访问时间 */
    @Column(name = "last_access_time")
    private Date lastAccessTime;

    /** 超时时间，单位为分钟 */
    @Column(name = "expire_time")
    private Long expireTime;

    /** 在线状态 */
    private String status = OnlineEnum.ON_LINE.getValue();

    /** 备份的当前用户会话 */
    @Transient //不在数据库中做映射
    private OnlineSession session;

}
