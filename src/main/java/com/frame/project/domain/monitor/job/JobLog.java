package com.frame.project.domain.monitor.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.frame.framework.aspectj.lang.annotation.Excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：sys_job_log
 * @describe ：定时任务调度信息日志
 */
@Entity
@Table(name = "sys_job_log")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_job_log",comment = "定时任务调度信息日志表")
public class JobLog implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    
    /** 任务名称 */
    @Excel(name = "任务名称")
    @Column(name = "job_name")
    private String jobName;

    /** 任务组名 */
    @Excel(name = "任务组名")
    @Column(name = "job_group")
    private String jobGroup;

    /** 调用目标字符串 */
    @Excel(name = "调用目标字符串")
    @Column(name = "invoke_target")
    private String invokeTarget;

    /** 日志信息 */
    @Excel(name = "日志信息")
    @Column(name = "job_message")
    private String jobMessage;

    /** 执行状态（0正常 1失败） */
    @Excel(name = "执行状态", readConverterExp = "0=正常,1=失败")
    private String status;

    /** 异常信息 */
    @Excel(name = "异常信息")
    @Column(name = "exception_info")
    private String exceptionInfo;

    /**
	 * 创建时间
	 */
	@Column(name = "create_Time")
	private Date createTime = new Date();
	
	/** 开始时间 */
	@Transient //不在数据库中做映射
    private Date startTime;

    /** 结束时间 */
	@Transient //不在数据库中做映射
    private Date endTime;

	/**
   	 * 状态 0：激活 1：冻结
   	 */
   	@Column(name = "remove_status")
   	private Integer removeStatus;
}
