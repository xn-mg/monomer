package com.frame.project.domain.monitor.operlog;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.frame.framework.aspectj.lang.annotation.Excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：OperLog
 * @describe ：操作日志记录表
 */
@Entity
@Table(name = "sys_oper_log")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_oper_log",comment = "操作日志记录表")
public class OperLog implements Serializable{
    private static final long serialVersionUID = 1L;


    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    
    /** 操作模块 */
    @Excel(name = "操作模块")
    private String title;

    /** 业务类型 */
    @Excel(name = "业务类型", readConverterExp = "0=其它,1=新增,2=修改,3=删除,4=授权,5=导出,6=导入,7=强退,8=生成代码,9=清空数据")
    @Column(name = "business_type")
    private Integer businessType;
    
    /** 业务类型数组 */
    @Transient //不在数据库中做映射
    private Integer[] businessTypes;

    /** 请求方法 */
    @Excel(name = "请求方法")
    private String method;

    /** 请求方式 */
    @Excel(name = "请求方式")
    @Column(name = "request_method")
    private String requestMethod;

    /** 操作人类别 */
    @Excel(name = "操作类别", readConverterExp = "0=其它,1=后台用户,2=手机端用户")
    @Column(name = "operator_type")
    private Integer operatorType;

    /** 操作人员 */
    @Excel(name = "操作人员")
    @Column(name = "oper_name")
    private String operName;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @Column(name = "dept_name")
    private String deptName;

    /** 请求url */
    @Excel(name = "请求地址")
    @Column(name = "oper_url")
    private String operUrl;

    /** 操作地址 */
    @Excel(name = "操作地址")
    @Column(name = "oper_ip")
    private String operIp;

    /** 操作地点 */
    @Excel(name = "操作地点")
    @Column(name = "oper_location")
    private String operLocation;

    /** 请求参数 */
    @Excel(name = "请求参数")
    @Column(name = "oper_param",columnDefinition = "longtext", nullable = true)
    private String operParam;

    /** 返回参数 */
    @Excel(name = "返回参数")
    @Column(name = "json_result")
    private String jsonResult;

    /** 状态0正常 1异常 */
    @Excel(name = "状态", readConverterExp = "0=正常,1=异常")
    private Integer status;

    /** 错误消息 */
    @Excel(name = "错误消息")
    @Column(name = "error_msg")
    private String errorMsg;

    /** 操作时间 */
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "oper_time")
    private Date operTime;
    
    /**
	 * 状态 0：激活 1：冻结
	 */
	@Column(name = "remove_status")
	private Integer removeStatus;
	
	
	/** 页面请求参数 */
	@Transient //不在数据库中做映射
    private Map<String, Object> params;

}
