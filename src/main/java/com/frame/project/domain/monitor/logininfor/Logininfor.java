package com.frame.project.domain.monitor.logininfor;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.frame.framework.aspectj.lang.annotation.Excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：Logininfor
 * @describe ：系统访问记录表
 */
@Entity
@Table(name = "sys_logininfor")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_logininfor",comment = "系统访问记录表")
public class Logininfor implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    
    /** 用户账号 */
    @Excel(name = "用户账号")
    @Column(name = "login_name")
    private String loginName;

    /** 登录状态 0成功 1失败 */
    @Excel(name = "登录状态", readConverterExp = "0=成功,1=失败")
    private String status;

    /** 登录IP地址 */
    @Excel(name = "登录地址")
    private String ipaddr;

    /** 登录地点 */
    @Excel(name = "登录地点")
    @Column(name = "login_location")
    private String loginLocation;

    /** 浏览器类型 */
    @Excel(name = "浏览器")
    private String browser;

    /** 操作系统 */
    @Excel(name = "操作系统")
    private String os;

    /** 提示消息 */
    @Excel(name = "提示消息")
    private String msg;

    /** 访问时间 */
    @Excel(name = "访问时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "login_time")
    private Date loginTime;
    
    /**
   	 * 状态 0：激活 1：冻结
   	 */
   	@Column(name = "remove_status")
   	private Integer removeStatus;
   	
   	/** 页面请求参数 */
	@Transient //不在数据库中做映射
    private Map<String, Object> params;
}