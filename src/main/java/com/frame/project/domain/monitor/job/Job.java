package com.frame.project.domain.monitor.job;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.frame.common.constant.ScheduleConstants;
import com.frame.common.utils.StringUtils;
import com.frame.framework.aspectj.lang.annotation.Excel;
import com.frame.framework.utils.quartz.CronUtils;
import com.frame.framework.web.domain.BaseEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @className ：Job
 * @describe ：定时任务调度信息
 */
@Entity
@Table(name = "sys_job")
@Getter
@Setter
@ToString(callSuper=true)
@org.hibernate.annotations.Table(appliesTo = "sys_job",comment = "定时任务调度信息表")
public class Job extends BaseEntity{
	
    private static final long serialVersionUID = 1L;


    /** 任务名称 */
    @Excel(name = "任务名称")
    @NotBlank(message = "任务名称不能为空")
    @Size(min = 0, max = 64, message = "任务名称不能超过64个字符")
    @Column(name = "job_name")
    private String jobName;

    /** 任务组名 */
    @Excel(name = "任务组名")
    @Column(name = "job_group")
    private String jobGroup;

    /** 调用目标字符串 */
    @Excel(name = "调用目标字符串")
    @NotBlank(message = "调用目标字符串不能为空")
    @Size(min = 0, max = 1000, message = "调用目标字符串长度不能超过500个字符")
    @Column(name = "invoke_target")
    private String invokeTarget;

    /** cron执行表达式 */
    @Excel(name = "执行表达式 ")
    @NotBlank(message = "Cron执行表达式不能为空")
    @Size(min = 0, max = 255, message = "Cron执行表达式不能超过255个字符")
    @Column(name = "cron_expression")
    private String cronExpression;

    /** cron计划策略 */
    @Excel(name = "计划策略 ", readConverterExp = "0=默认,1=立即触发执行,2=触发一次执行,3=不触发立即执行")
    @Column(name = "misfire_policy")
    private String misfirePolicy = ScheduleConstants.MISFIRE_DEFAULT;

    /** 是否并发执行（0允许 1禁止） */
    @Excel(name = "并发执行", readConverterExp = "0=允许,1=禁止")
    private String concurrent;

    /** 任务状态（0正常 1暂停） */
    @Excel(name = "任务状态", readConverterExp = "0=正常,1=暂停")
    private String status;

    /**
          * 下次执行时间
     */
    @Transient //不在数据库中做映射
    private Date nextValidTime;
    
    public Date getNextValidTime()
    {
        if (StringUtils.isNotEmpty(cronExpression))
        {
            return CronUtils.getNextExecution(cronExpression);
        }
        return null;
    }
}