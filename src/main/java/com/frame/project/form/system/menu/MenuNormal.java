package com.frame.project.form.system.menu;

import java.util.Date;

import lombok.Data;

@Data
public class MenuNormal {

	private Long id;
	
	private Long parentId;
	
	private String menuName;
	
	private String url;
	
	private String visible;
	
	private String perms;
	
	private String target;
	
	private String menuType;
	
	private String icon;
	
	private Integer orderNum;
	
	private Date createTime;
}
