package com.frame.project.repository.monitor.logininfor;

import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.monitor.logininfor.Logininfor;

/**
 * 
 * @className ：LogininforRepository
 * @describe ：系统访问记录
 */
@Repository
public interface LogininforRepository extends BaseRepository<Logininfor, Long> {

}
