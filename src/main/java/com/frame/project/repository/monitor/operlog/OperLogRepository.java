package com.frame.project.repository.monitor.operlog;

import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.monitor.operlog.OperLog;

/**
 * 
 * @className ：OperLogRepository
 * @describe ：操作日志记录
 */
@Repository
public interface OperLogRepository extends BaseRepository<OperLog, Long> {

}
