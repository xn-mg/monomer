package com.frame.project.repository.monitor.job;

import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.monitor.job.Job;

/**
 * 
 * @className ：JobRepository
 * @describe ：定时任务调度信息
 */
@Repository
public interface JobRepository extends BaseRepository<Job, Long> {

}
