package com.frame.project.repository.monitor.job;

import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.monitor.job.JobLog;

/**
 * 
 * @className ：JobLogRepository
 * @describe ：定时任务调度信息日志
 */
@Repository
public interface JobLogRepository extends BaseRepository<JobLog, Long>{

}
