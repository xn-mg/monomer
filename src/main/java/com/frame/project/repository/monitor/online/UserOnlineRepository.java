package com.frame.project.repository.monitor.online;

import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.monitor.online.UserOnline;

/**
 * 
 * @className ：UserOnlineRepository
 * @describe ：当前在线会话
 */
@Repository
public interface UserOnlineRepository extends BaseRepository<UserOnline, Long> {

	UserOnline findBySessionId(String sessionId);
	
}
