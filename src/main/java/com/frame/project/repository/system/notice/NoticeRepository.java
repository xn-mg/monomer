package com.frame.project.repository.system.notice;

import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.notice.Notice;

/**
 * 
 * @className ：NoticeRepository
 * @describe ：通知公告
 */
@Repository
public interface NoticeRepository extends BaseRepository<Notice, Long>{

}
