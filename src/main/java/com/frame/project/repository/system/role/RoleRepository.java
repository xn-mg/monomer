package com.frame.project.repository.system.role;

import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.role.Role;

/**
 * 
 * @className ：RoleRepository
 * @describe ：角色
 */
@Repository
public interface RoleRepository extends BaseRepository<Role, Long>{
	
	Role findByRoleName(String roleName);
	
	Role findByRoleKey(String roleKey);
}
