package com.frame.project.repository.system.user;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.user.User;

/**
 * 
 * @className ：UserRepository
 * @describe ：用户
 */
@Repository
public interface UserRepository extends BaseRepository<User, Long>{

	@Query("select count(1) from User u where u.dept.id = ?1 and u.removeStatus = 0")
	Integer checkDeptExistUser(Long deptId);
	
	@Query("from User u where u.id = ?1")
	User findByUserId(Long userId);
	
	User findByUserName(String userName);
	
	User findByPhonenumber(String phonenumber);
	
	User findByEmail(String email);
	
	User findByLoginName(String loginName);
	
	@Query(nativeQuery = true, value = "select count(1) from sys_user where login_name = ?1")
	Integer checkLoginNameUnique(String loginName);
	
	@Query("from User u where u.phonenumber = ?1")
	User checkPhoneUnique(String phonenumber);
	
	@Query("from User u where u.email = ?1")
	User checkEmailUnique(String email);
}
