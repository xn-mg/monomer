package com.frame.project.repository.system.config;

import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.config.Config;

/**
 * 
 * @className ：ConfigRepository
 * @describe ：参数配置
 */
@Repository
public interface ConfigRepository extends BaseRepository<Config, Long> {

	Config findByConfigKey(String configKey);
	
}
