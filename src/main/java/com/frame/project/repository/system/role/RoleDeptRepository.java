package com.frame.project.repository.system.role;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.role.RoleDept;

/**
 * 
 * @className ：RoleDeptRepository
 * @describe ：角色和部门
 */
@Repository
public interface RoleDeptRepository extends BaseRepository<RoleDept, Long>{

	/**
          * 查询部门使用数量
     * 
     * @param deptId 部门ID
     * @return 结果
     */
	@Query(nativeQuery = true, value = "select count(1) from sys_role_dept where dept_id=?1")
	Integer selectCountRoleDeptByDeptId(Long deptId);
	
	/**
     * 通过角色ID删除角色和部门关联
     * 
     * @param roleId 角色ID
     * @return 结果
     */
	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "delete from sys_role_dept where role_id=?1")
	void deleteRoleDeptByRoleId(Long roleId);
}
