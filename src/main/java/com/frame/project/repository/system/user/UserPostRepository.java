package com.frame.project.repository.system.user;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.user.UserPost;

/**
 * 
 * @className ：UserPostRepository
 * @describe ：用户和岗位
 */
@Repository
public interface UserPostRepository extends BaseRepository<UserPost, Long>{

	List<UserPost> findByUser_Id(Long userId);
	
	/**
          * 通过岗位ID查询岗位使用数量
     * 
     * @param postId 岗位ID
     * @return 结果
     */
	@Query("select count(1) from UserPost up where up.post.id = ?1")
	Integer countUserPostById(Long postId);
	
	/**
         * 通过用户ID删除用户和岗位关联
     * 
     * @param userId 用户ID
     * @return 结果
     */
	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "delete from sys_user_post where user_id=?1")
    void deleteUserPostByUserId(Long userId);
	
	
}
