package com.frame.project.repository.system.role;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.role.RoleMenu;

/**
 * 
 * @className ：RoleMenuRepository
 * @describe ：角色和菜单
 */
@Repository
public interface RoleMenuRepository extends BaseRepository<RoleMenu, Long>{

	/**
	 * 根据menu_id查询数量
	 * @param menuId
	 * @return
	 */
	@Query(nativeQuery = true, value = "select count(1) from sys_role_menu where menu_id=?1")
	Integer selectCountRoleMenuByMenuId(Long menuId);
	
	/**
          * 通过角色ID删除角色和菜单关联
     * 
     * @param roleId 角色ID
     * @return 结果
     */
	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "delete from sys_role_menu where role_id=?1")
    void deleteRoleMenuByRoleId(Long roleId);
	
}
