package com.frame.project.repository.system.dept;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.dept.Dept;

/**
 * 
 * @className ：DeptRepository
 * @describe ：部门
 */
@Repository
public interface DeptRepository extends BaseRepository<Dept, Long> {

	@Query(nativeQuery = true, value = "select concat(d.id, d.dept_name) as dept_name from sys_dept d"
			+ " left join sys_role_dept rd on d.id = rd.dept_id where d.remove_status = 0"
			+ " and rd.role_id = ?1 order by d.parent_id, d.order_num")
	List<String> selectRoleDeptTree(Long roleId);
	
	@Query("select count(1) from Dept d where d.removeStatus = 0 and d.parentId = ?1")
	Integer selectDeptCountByParent(Long parentId);
	
	@Query(nativeQuery = true, value = "select * from sys_dept where find_in_set(?1, ancestors)")
	List<Dept> selectChildrenDeptById(Long deptId);
	
	@Query(nativeQuery = true, value = "select count(*) from sys_dept where status = 0 and remove_status = 0 and find_in_set(?1, ancestors)")
	Integer selectNormalChildrenDeptById(Long deptId);
	
	Dept findByParentId(Long parentId);
	
	Dept findByDeptNameAndParentId(String deptName, Long parentId);
	
}
