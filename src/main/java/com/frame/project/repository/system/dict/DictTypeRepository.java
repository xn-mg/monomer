package com.frame.project.repository.system.dict;

import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.dict.DictType;

/**
 * 
 * @className ：DictTypeRepository
 * @describe ：字典类型
 */
@Repository
public interface DictTypeRepository extends BaseRepository<DictType, Long>{

	DictType findByDictTypes(String dictType);
	
}
