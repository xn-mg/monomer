package com.frame.project.repository.system.user;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.user.UserRole;

/**
 * 
 * @className ：UserRoleRepository
 * @describe ：用户和角色
 */
@Repository
public interface UserRoleRepository extends BaseRepository<UserRole, Long>{

	/**
     * 通过用户ID查询用户和角色关联
     * 
     * @param userId 用户ID
     * @return 用户和角色关联列表
     */
    List<UserRole> findByUser_Id(Long userId);
	
	/**
     * 通过用户ID删除用户和角色关联
     * 
     * @param userId 用户ID
     * @return 结果
     */
    @Modifying
	@Transactional
	@Query(nativeQuery = true, value = "delete from sys_user_role where user_id = ?1")
    void deleteUserRoleByUserId(Long userId);
	
	/**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
	@Query(nativeQuery = true, value = "select count(1) from sys_user_role where role_id = ?1")
    Integer countUserRoleByRoleId(Long roleId);
	
	/**
     * 删除用户和角色关联信息
     * 
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "delete from sys_user_role where user_id=?1 and role_id=?2")
    void deleteUserRolebyUserIdAndRoleId(Long userId, Long roleId);
	
	/**
	 * 根据roleId查询集合
	 * @param roleId
	 * @return
	 */
	List<UserRole> findByRole_Id(Long roleId);
}
