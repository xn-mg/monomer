package com.frame.project.repository.system.post;

import org.springframework.stereotype.Repository;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.post.Post;

/**
 * 
 * @className ：PostRepository
 * @describe ：岗位
 */
@Repository
public interface PostRepository extends BaseRepository<Post, Long>{

	Post findByPostName(String postName);
	
	Post findByPostCode(String postCode);
	
	
}
