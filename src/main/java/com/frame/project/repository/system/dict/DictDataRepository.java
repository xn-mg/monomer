package com.frame.project.repository.system.dict;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.frame.framework.web.repository.BaseRepository;
import com.frame.project.domain.system.dict.DictData;

/**
 * 
 * @className ：DictDataRepository
 * @describe ：字典数据
 */
@Repository
public interface DictDataRepository extends BaseRepository<DictData, Long>{

	DictData findByDictTypeAndDictValue(String dictType, String dictValue);
	
	@Query("from DictData dd where dd.dictType = ?1 and dd.status = 0 and dd.removeStatus =0")
	List<DictData> findDictDataByType(String dictType);
	
	@Query("select count(1) from DictData dd where dd.dictType = ?1 and dd.removeStatus =0")
	Integer countDictDataByType(String dictType);
	
	@Modifying
	@Transactional
	@Query("update DictData set dictType = ?1 where dictType = ?2")
	void updateDictDataType(String newDictTypeString, String oldDictTypeString);
	
	List<DictData> findByDictType(String dictType);
}
