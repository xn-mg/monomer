package com.frame.project.controller.system.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.frame.common.utils.StringUtils;
import com.frame.framework.shiro.service.RegisterService;
import com.frame.framework.web.controller.BaseController;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.error.enums.WebEnum;
import com.frame.project.domain.system.user.User;
import com.frame.project.service.system.config.IConfigService;

/**
 * 
 * @className ：RegisterController
 * @describe ：注册验证
 */
@Controller
public class RegisterController extends BaseController
{
    @Autowired
    private RegisterService registerService;

    @Autowired
    private IConfigService configService;

    @GetMapping("/register")
    public String register()
    {
        return "register";
    }

    @PostMapping("/register")
    @ResponseBody
    public WebError ajaxRegister(User user)
    {
        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser"))))
        {
            return error(WebEnum.NOT_CURRENT_SYSTEM);
        }
        String msg = registerService.register(user);
        
        return StringUtils.isEmpty(msg) ?  success() : error(-1,msg);
    }

}
