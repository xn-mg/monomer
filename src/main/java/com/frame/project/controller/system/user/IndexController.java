package com.frame.project.controller.system.user;

import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.frame.common.constant.ShiroConstants;
import com.frame.common.utils.CookieUtils;
import com.frame.common.utils.DateUtils;
import com.frame.common.utils.ServletUtils;
import com.frame.common.utils.StringUtils;
import com.frame.common.utils.security.ShiroUtils;
import com.frame.common.utils.text.Convert;
import com.frame.framework.config.MyFrameConfig;
import com.frame.framework.shiro.service.PasswordService;
import com.frame.framework.web.controller.BaseController;
import com.frame.framework.web.error.WebError;
import com.frame.project.domain.system.menu.Menu;
import com.frame.project.domain.system.user.User;
import com.frame.project.service.system.config.IConfigService;
import com.frame.project.service.system.menu.MenuService;

/**
 * 
 * @className ：IndexController
 * @describe ：首页 业务处理
 */
@Controller
public class IndexController extends BaseController
{
    @Autowired
    private MenuService menuService;

    @Autowired
    private IConfigService configService;

    @Autowired
    private MyFrameConfig myFrameConfig;
    
    @Autowired
    private PasswordService passwordService;

    // 系统首页
    @GetMapping("/index")
    public String index(Model model)
    {
        // 取身份信息
        User user = ShiroUtils.getSysUser();
     // 根据用户id取出菜单
        List<Menu> menus = menuService.selectMenusByUser(user);
        model.addAttribute("menus", menus);
        model.addAttribute("user", user);
        model.addAttribute("sideTheme", configService.selectConfigByKey("sys.index.sideTheme"));
        model.addAttribute("skinName", configService.selectConfigByKey("sys.index.skinName"));
        Boolean footer = Convert.toBool(configService.selectConfigByKey("sys.index.footer"), true);
        Boolean tagsView = Convert.toBool(configService.selectConfigByKey("sys.index.tagsView"), true);
        model.addAttribute("footer", footer);
        model.addAttribute("tagsView", tagsView);
        model.addAttribute("mainClass", contentMainClass(footer, tagsView));
        
        model.addAttribute("copyrightYear", MyFrameConfig.getCopyrightYear());
        model.addAttribute("isDefaultModifyPwd", initPasswordIsModify(user.getPwdUpdateDate()));
        model.addAttribute("isPasswordExpired", passwordIsExpiration(user.getPwdUpdateDate()));
        model.addAttribute("isMobile", ServletUtils.checkAgentIsMobile(ServletUtils.getRequest().getHeader("User-Agent")));

        // 菜单导航显示风格
        String menuStyle = configService.selectConfigByKey("sys.index.menuStyle");
        // 移动端，默认使左侧导航菜单，否则取默认配置
        String indexStyle = ServletUtils.checkAgentIsMobile(ServletUtils.getRequest().getHeader("User-Agent")) ? "index" : menuStyle;
        
        // 优先Cookie配置导航菜单
        Cookie[] cookies = ServletUtils.getRequest().getCookies();
        for (Cookie cookie : cookies)
        {
            if (StringUtils.isNotEmpty(cookie.getName()) && "nav-style".equalsIgnoreCase(cookie.getName()))
            {
                indexStyle = cookie.getValue();
                break;
            }
        }
        String webIndex = "topnav".equalsIgnoreCase(indexStyle) ? "index-topnav" : "index";
        return webIndex;
    }

    //锁定屏幕
    @GetMapping("/lockscreen")
    public String lockscreen(Model model)
    {
    	model.addAttribute("user", getSysUser());
        ServletUtils.getSession().setAttribute(ShiroConstants.LOCK_SCREEN, true);
        return "lock";
    }

    // 解锁屏幕
    @PostMapping("/unlockscreen")
    @ResponseBody
    public WebError unlockscreen(String password)
    {
        User user = getSysUser();
        if (StringUtils.isNull(user))
        {
            return error(-1,"服务器超时，请重新登陆");
        }
        if (passwordService.matches(user, password))
        {
            ServletUtils.getSession().removeAttribute(ShiroConstants.LOCK_SCREEN);
            return success();
        }
        return error(-1,"密码不正确，请重新输入。");
    }
    
    // 切换主题
    @GetMapping("/system/switchSkin")
    public String switchSkin(Model model)
    {
        return "skin";
    }

    //切换菜单
    @GetMapping("/system/menuStyle/{style}")
    @ResponseBody
    public WebError menuStyle(@PathVariable String style, HttpServletResponse response)
    {
        CookieUtils.setCookie(response, "nav-style", style);
        return success();
    }
    
    // 系统介绍
    @GetMapping("/system/main")
    public String main(Model model)
    {
    	model.addAttribute("version", myFrameConfig.getVersion());
        return "main";
    }
    
    // content-main class
    public String contentMainClass(Boolean footer, Boolean tagsView)
    {
        if (!footer && !tagsView)
        {
            return "tagsview-footer-hide";
        }
        else if (!footer)
        {
            return "footer-hide";
        }
        else if (!tagsView)
        {
            return "tagsview-hide";
        }
        return StringUtils.EMPTY;
    }
    
 // 检查初始密码是否提醒修改
    public boolean initPasswordIsModify(Date pwdUpdateDate)
    {
        Integer initPasswordModify = Convert.toInt(configService.selectConfigByKey("sys.account.initPasswordModify"));
        return initPasswordModify != null && initPasswordModify == 1 && pwdUpdateDate == null;
    }
    
    // 检查密码是否过期
    public boolean passwordIsExpiration(Date pwdUpdateDate)
    {
        Integer passwordValidateDays = Convert.toInt(configService.selectConfigByKey("sys.account.passwordValidateDays"));
        if (passwordValidateDays !=null && passwordValidateDays > 0)
        {
            if (StringUtils.isNull(pwdUpdateDate))
            {
                // 如果从未修改过初始密码，直接提醒过期
                return true;
            }
            Date nowDate = DateUtils.getNowDate();
            return DateUtils.differentDaysByMillisecond(nowDate, pwdUpdateDate) > passwordValidateDays;
        }
        return false;
    }
}
