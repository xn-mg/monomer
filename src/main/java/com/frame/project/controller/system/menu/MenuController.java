package com.frame.project.controller.system.menu;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.frame.common.constant.UserConstants;
import com.frame.framework.aspectj.lang.annotation.Log;
import com.frame.framework.aspectj.lang.enums.BusinessType;
import com.frame.framework.web.controller.BaseController;
import com.frame.framework.web.domain.Ztree;
import com.frame.framework.web.error.WebError;
import com.frame.project.domain.system.menu.Menu;
import com.frame.project.domain.system.role.Role;
import com.frame.project.service.system.menu.MenuService;

/**
 * 
 * @className ：MenuController
 * @describe ：菜单信息
 */
@Controller
@RequestMapping("/system/menu")
public class MenuController extends BaseController
{
    private String prefix = "system/menu";

    @Autowired
    private MenuService menuService;

    @RequiresPermissions("system:menu:view")
    @GetMapping()
    public String menu()
    {
        return prefix + "/menu";
    }

    @RequiresPermissions("system:menu:list")
    @PostMapping("/list")
    @ResponseBody
    public List<Menu> list(Menu menu)
    {
        List<Menu> menuList = menuService.selectMenuList(menu);
        return menuList;
    }

    /**
     * 删除菜单
     */
    @Log(title = "菜单管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:menu:remove")
    @GetMapping("/remove/{menuId}")
    @ResponseBody
    public WebError remove(@PathVariable("menuId") Long menuId)
    {
        if (menuService.selectCountMenuByParentId(menuId) > 0)
        {
            return error(-1,"存在子菜单,不允许删除");
        }
        if (menuService.selectCountRoleMenuByMenuId(menuId) > 0)
        {
            return error(-1,"菜单已分配,不允许删除");
        }
        menuService.deleteMenuById(menuId);
        return success();
    }

    /**
     * 新增
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") Long parentId, Model model)
    {
        Menu menu = null;
        if (0L != parentId)
        {
            menu = menuService.findById(parentId);
        }
        else
        {
            menu = new Menu();
            menu.setId(0L);
            menu.setMenuName("主目录");
        }
        model.addAttribute("menu", menu);
        return prefix + "/add";
    }

    /**
     * 新增保存菜单
     */
    @Log(title = "菜单管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:menu:add")
    @PostMapping("/add")
    @ResponseBody
    public WebError addSave(@Validated Menu menu)
    {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu)))
        {
            return error(-1,"新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        menuService.insertMenu(menu);
        return success();
    }

    /**
     * 修改菜单
     */
    @GetMapping("/edit/{menuId}")
    public String edit(@PathVariable("menuId") Long menuId, Model model)
    {
    	Menu menu = menuService.findById(menuId);
    	if(menu.getParentId() != 0L) {
    		Menu parent = menuService.findById(menu.getParentId());
    		menu.setParentName(parent.getMenuName());
    	}
    	model.addAttribute("menu", menu);
        return prefix + "/edit";
    }

    /**
     * 修改保存菜单
     */
    @Log(title = "菜单管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:menu:edit")
    @PostMapping("/edit")
    @ResponseBody
    public WebError editSave(@Validated Menu menu)
    {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu)))
        {
            return error(-1,"修改菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        menuService.updateMenu(menu);
        return success();
    }

    /**
     * 选择菜单图标
     */
    @GetMapping("/icon")
    public String icon()
    {
        return prefix + "/icon";
    }

    /**
     * 校验菜单名称
     */
    @PostMapping("/checkMenuNameUnique")
    @ResponseBody
    public String checkMenuNameUnique(Menu menu)
    {
        return menuService.checkMenuNameUnique(menu);
    }

    /**
     * 加载角色菜单列表树
     */
    @GetMapping("/roleMenuTreeData")
    @ResponseBody
    public List<Ztree> roleMenuTreeData(Role role)
    {
        List<Ztree> ztrees = menuService.roleMenuTreeData(role);
        return ztrees;
    }

    /**
     * 加载所有菜单列表树
     */
    @GetMapping("/menuTreeData")
    @ResponseBody
    public List<Ztree> menuTreeData(Role role)
    {
        List<Ztree> ztrees = menuService.menuTreeData();
        return ztrees;
    }

    /**
     * 选择菜单树
     */
    @GetMapping("/selectMenuTree/{menuId}")
    public String selectMenuTree(@PathVariable("menuId") Long menuId, Model model)
    {
    	model.addAttribute("menu", menuService.findById(menuId));
        return prefix + "/tree";
    }
}