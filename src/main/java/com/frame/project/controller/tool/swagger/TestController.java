package com.frame.project.controller.tool.swagger;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.frame.common.utils.StringUtils;
import com.frame.framework.web.controller.BaseController;
import com.frame.framework.web.error.WebError;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;



/**
 * swagger 用户测试方法
 * 
 */
@Api("用户信息管理")
@RestController
@RequestMapping("/test/user")
public class TestController extends BaseController
{
    private final static Map<Integer, TestUserEntity> users = new LinkedHashMap<Integer, TestUserEntity>();
    {
        users.put(1, new TestUserEntity(1, "admin", "admin123", "15888888888"));
        users.put(2, new TestUserEntity(2, "ry", "admin123", "15666666666"));
    }

    @ApiOperation(nickname = "获取用户列表", httpMethod = "GET", value = "获取用户列表")
    @GetMapping("/list")
    public WebError userList()
    {
    	WebError web = new WebError();
        List<TestUserEntity> userList = new ArrayList<TestUserEntity>(users.values());
        web.putData("userList", userList);
        return web;
    }

    @ApiOperation("获取用户详细")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "int", paramType = "path")
    @GetMapping("/{userId}")
    public WebError getUser(@PathVariable Integer userId)
    {
    	WebError web = new WebError();
        if (!users.isEmpty() && users.containsKey(userId))
        {
        	web.putData("user", users.get(userId));
            return web;
        }
        else
        {
            return error(-1,"用户不存在");
        }
    }

    @ApiOperation("新增用户")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "userId", value = "用户id", dataType = "Integer"),
        @ApiImplicitParam(name = "username", value = "用户名称", dataType = "String"),
        @ApiImplicitParam(name = "password", value = "用户密码", dataType = "String"),
        @ApiImplicitParam(name = "mobile", value = "用户手机", dataType = "String")
    })
    @PostMapping("/save")
    public WebError save(TestUserEntity user)
    {
    	WebError web = new WebError();
        if (StringUtils.isNull(user) || StringUtils.isNull(user.getUserId()))
        {
            return error(-1,"用户ID不能为空");
        }
        web.putData("user", users.put(user.getUserId(), user));
        return web;
    }

    @ApiOperation("更新用户")
    @PutMapping("/update")
    public WebError update(@RequestBody TestUserEntity user)
    {
    	WebError web = new WebError();
        if (StringUtils.isNull(user) || StringUtils.isNull(user.getUserId()))
        {
            return error(-1,"用户ID不能为空");
        }
        if (users.isEmpty() || !users.containsKey(user.getUserId()))
        {
            return error(-1,"用户不存在");
        }
        users.remove(user.getUserId());
        web.putData("user", users.put(user.getUserId(), user));
        return web;
    }

    @ApiOperation("删除用户信息")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "int", paramType = "path")
    @DeleteMapping("/{userId}")
    public WebError delete(@PathVariable Integer userId)
    {
        if (!users.isEmpty() && users.containsKey(userId))
        {
            users.remove(userId);
            return success();
        }
        else
        {
            return error(-1,"用户不存在");
        }
    }
}

