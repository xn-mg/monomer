package com.frame.project.controller.monitor.logininfor;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.frame.common.utils.poi.ExcelUtil;
import com.frame.framework.aspectj.lang.annotation.Log;
import com.frame.framework.aspectj.lang.enums.BusinessType;
import com.frame.framework.shiro.service.PasswordService;
import com.frame.framework.web.controller.BaseController;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.page.TableDataInfo;
import com.frame.project.domain.monitor.logininfor.Logininfor;
import com.frame.project.service.monitor.logininfor.LogininforService;

/**
 * 
 * @className ：LogininforController
 * @describe ：系统访问记录
 */
@Controller
@RequestMapping("/monitor/logininfor")
public class LogininforController extends BaseController
{
    private String prefix = "monitor/logininfor";

    @Autowired
    private LogininforService logininforService;

    @Autowired
    private PasswordService passwordService;

    @RequiresPermissions("monitor:logininfor:view")
    @GetMapping()
    public String logininfor()
    {
        return prefix + "/logininfor";
    }

    @RequiresPermissions("monitor:logininfor:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Logininfor logininfor,@RequestParam(defaultValue = "0") Integer pageNum, @RequestParam(defaultValue = "10") Integer pageSize)
    {
        startPage();
        Page<Logininfor> findByPage = logininforService.findByPage(pageNum, pageSize, logininfor);
        return getDataTable(findByPage);
    }

    @Log(title = "导出日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("monitor:logininfor:export")
    @PostMapping("/export")
    @ResponseBody
    public WebError export(Logininfor logininfor)
    {
    	Page<Logininfor> page = logininforService.findByPage(0, Integer.MAX_VALUE, logininfor);
        ExcelUtil<Logininfor> util = new ExcelUtil<Logininfor>(Logininfor.class);
        return util.exportExcel(page.getContent(), "登陆日志");
    }

    @RequiresPermissions("monitor:logininfor:remove")
    @Log(title = "批量删除日志", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public WebError remove(String ids)
    {
    	logininforService.deleteLogininforByIds(ids);
        return success();
    }

    @RequiresPermissions("monitor:logininfor:remove")
    @Log(title = "清空日志", businessType = BusinessType.CLEAN)
    @PostMapping("/clean")
    @ResponseBody
    public WebError clean()
    {
        logininforService.cleanLogininfor();
        return success();
    }

    @RequiresPermissions("monitor:logininfor:unlock")
    @Log(title = "账户解锁", businessType = BusinessType.OTHER)
    @PostMapping("/unlock")
    @ResponseBody
    public WebError unlock(String loginName)
    {
        passwordService.clearLoginRecordCache(loginName);
        return success();
    }
}
