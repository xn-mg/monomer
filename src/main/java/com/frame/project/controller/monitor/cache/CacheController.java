package com.frame.project.controller.monitor.cache;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.frame.framework.web.controller.BaseController;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.service.CacheService;


/**
 * 缓存监控
 * 
 */
@Controller
@RequestMapping("/monitor/cache")
public class CacheController extends BaseController
{
    private String prefix = "monitor/cache";

    @Autowired
    private CacheService cacheService;

    @RequiresPermissions("monitor:cache:view")
    @GetMapping()
    public String cache(Model model)
    {
    	model.addAttribute("cacheNames", cacheService.getCacheNames());
        return prefix + "/cache";
    }

    @PostMapping("/getNames")
    public String getCacheNames(String fragment, Model model)
    {
    	model.addAttribute("cacheNames", cacheService.getCacheNames());
        return prefix + "/cache::" + fragment;
    }

    @PostMapping("/getKeys")
    public String getCacheKeys(String fragment, String cacheName, Model model)
    {
    	model.addAttribute("cacheName", cacheName);
    	model.addAttribute("cacheKeys", cacheService.getCacheKeys(cacheName));
        return prefix + "/cache::" + fragment;
    }

    @PostMapping("/getValue")
    public String getCacheValue(String fragment, String cacheName, String cacheKey, Model model)
    {
    	model.addAttribute("cacheName", cacheName);
    	model.addAttribute("cacheKey", cacheKey);
    	model.addAttribute("cacheValue", cacheService.getCacheValue(cacheName, cacheKey));
        return prefix + "/cache::" + fragment;
    }

    @PostMapping("/clearCacheName")
    @ResponseBody
    public WebError clearCacheName(String cacheName)
    {
        cacheService.clearCacheName(cacheName);
        return success();
    }

    @PostMapping("/clearCacheKey")
    @ResponseBody
    public WebError clearCacheKey(String cacheName, String cacheKey)
    {
        cacheService.clearCacheKey(cacheName, cacheKey);
        return success();
    }

    @GetMapping("/clearAll")
    @ResponseBody
    public WebError clearAll()
    {
        cacheService.clearAll();
        return success();
    }
}
