package com.frame.project.controller.monitor.job;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.frame.common.constant.Constants;
import com.frame.common.exception.job.TaskException;
import com.frame.common.utils.StringUtils;
import com.frame.common.utils.poi.ExcelUtil;
import com.frame.framework.aspectj.lang.annotation.Log;
import com.frame.framework.aspectj.lang.enums.BusinessType;
import com.frame.framework.utils.quartz.CronUtils;
import com.frame.framework.web.controller.BaseController;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.page.TableDataInfo;
import com.frame.project.domain.monitor.job.Job;
import com.frame.project.service.monitor.job.JobService;

/**
 * 
 * @className ：JobController
 * @describe ：调度任务信息操作处理
 */
@Controller
@RequestMapping("/monitor/job")
public class JobController extends BaseController
{
    private String prefix = "monitor/job";

    @Autowired
    private JobService jobService;

    @RequiresPermissions("monitor:job:view")
    @GetMapping()
    public String job()
    {
        return prefix + "/job";
    }

    @RequiresPermissions("monitor:job:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Job job,@RequestParam(defaultValue = "0") Integer pageNum, @RequestParam(defaultValue = "10") Integer pageSize)
    {
        startPage();
        Page<Job> findByPage = jobService.findByPage(pageNum, pageSize, job);
        return getDataTable(findByPage);
    }

    @Log(title = "定时任务", businessType = BusinessType.EXPORT)
    @RequiresPermissions("monitor:job:export")
    @PostMapping("/export")
    @ResponseBody
    public WebError export(Job job)
    {
        Page<Job> page = jobService.findByPage(0, Integer.MAX_VALUE, job);
        ExcelUtil<Job> util = new ExcelUtil<Job>(Job.class);
        return util.exportExcel(page.getContent(), "定时任务");
    }

    @Log(title = "定时任务", businessType = BusinessType.DELETE)
    @RequiresPermissions("monitor:job:remove")
    @PostMapping("/remove")
    @ResponseBody
    public WebError remove(String ids) throws SchedulerException
    {
        jobService.deleteJobByIds(ids);
        return success();
    }

    @RequiresPermissions("monitor:job:detail")
    @GetMapping("/detail/{jobId}")
    public String detail(@PathVariable("jobId") Long jobId, Model model)
    {
    	model.addAttribute("name", "job");
    	model.addAttribute("job", jobService.findById(jobId));
        return prefix + "/detail";
    }

    /**
     * 任务调度状态修改
     */
    @Log(title = "定时任务", businessType = BusinessType.UPDATE)
    @RequiresPermissions("monitor:job:changeStatus")
    @PostMapping("/changeStatus")
    @ResponseBody
    public WebError changeStatus(Job job) throws SchedulerException
    {
        Job newJob = jobService.findById(job.getId());
        newJob.setStatus(job.getStatus());
        jobService.changeStatus(newJob);
        return success();
    }

    /**
     * 任务调度立即执行一次
     */
    @Log(title = "定时任务", businessType = BusinessType.UPDATE)
    @RequiresPermissions("monitor:job:changeStatus")
    @PostMapping("/run")
    @ResponseBody
    public WebError run(Job job) throws SchedulerException
    {
    	boolean result = jobService.run(job);
        return result ? success() : error(-1 ,"任务不存在或已过期！");
    }

    /**
     * 新增调度
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存调度
     */
    @Log(title = "定时任务", businessType = BusinessType.INSERT)
    @RequiresPermissions("monitor:job:add")
    @PostMapping("/add")
    @ResponseBody
    public WebError addSave(@Validated Job job) throws SchedulerException, TaskException
    {
    	if (!CronUtils.isValid(job.getCronExpression()))
        {
            return error(-1,"新增任务'" + job.getJobName() + "'失败，Cron表达式不正确");
        }
        else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_RMI))
        {
            return error(-1,"新增任务'" + job.getJobName() + "'失败，目标字符串不允许'rmi://'调用");
        }
        else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_LDAP))
        {
            return error(-1,"新增任务'" + job.getJobName() + "'失败，目标字符串不允许'ldap://'调用");
        }
        else if (StringUtils.containsAnyIgnoreCase(job.getInvokeTarget(),
                new String[] { Constants.HTTP, Constants.HTTPS }))
        {
            return error(-1,"新增任务'" + job.getJobName() + "'失败，目标字符串不允许'http(s)//'调用");
        }
    	jobService.insertJob(job);
        return success();
    }

    /**
     * 修改调度
     */
    @GetMapping("/edit/{jobId}")
    public String edit(@PathVariable("jobId") Long jobId, Model model)
    {
    	model.addAttribute("job", jobService.findById(jobId));
        return prefix + "/edit";
    }

    /**
     * 修改保存调度
     */
    @Log(title = "定时任务", businessType = BusinessType.UPDATE)
    @RequiresPermissions("monitor:job:edit")
    @PostMapping("/edit")
    @ResponseBody
    public WebError editSave(@Validated Job job) throws SchedulerException, TaskException
    {
    	if (!CronUtils.isValid(job.getCronExpression()))
        {
            return error(-1,"新增任务'" + job.getJobName() + "'失败，Cron表达式不正确");
        }
        else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_RMI))
        {
            return error(-1,"新增任务'" + job.getJobName() + "'失败，目标字符串不允许'rmi://'调用");
        }
        else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_LDAP))
        {
            return error(-1,"新增任务'" + job.getJobName() + "'失败，目标字符串不允许'ldap://'调用");
        }
        else if (StringUtils.containsAnyIgnoreCase(job.getInvokeTarget(),
                new String[] { Constants.HTTP, Constants.HTTPS }))
        {
            return error(-1,"新增任务'" + job.getJobName() + "'失败，目标字符串不允许'http(s)//'调用");
        }
    	jobService.updateJob(job);
        return success();
    }

    /**
     * Cron表达式在线生成
     */
    @GetMapping("/cron")
    public String cron()
    {
        return prefix + "/cron";
    }
    
    /**
     * 校验cron表达式是否有效
     */
    @PostMapping("/checkCronExpressionIsValid")
    @ResponseBody
    public boolean checkCronExpressionIsValid(Job job)
    {
        return jobService.checkCronExpressionIsValid(job.getCronExpression());
    }
    
    /**
     * 查询cron表达式近5次的执行时间
     */
    @GetMapping("/queryCronExpression")
    @ResponseBody
    public WebError queryCronExpression(
            @RequestParam(value = "cronExpression", required = false) String cronExpression)
    {
    	WebError web = new WebError();
        if (jobService.checkCronExpressionIsValid(cronExpression))
        {
            List<String> dateList = CronUtils.getRecentTriggerTime(cronExpression);
            web.putData("dateList", dateList);
            return web;
        }
        else
        {
            return error(-1,"表达式无效");
        }
    }
}
