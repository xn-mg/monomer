package com.frame.project.controller.monitor.online;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.frame.common.enumutil.OnlineEnum;
import com.frame.common.utils.security.ShiroUtils;
import com.frame.framework.aspectj.lang.annotation.Log;
import com.frame.framework.aspectj.lang.enums.BusinessType;
import com.frame.framework.shiro.session.OnlineSessionDAO;
import com.frame.framework.web.controller.BaseController;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.page.TableDataInfo;
import com.frame.project.domain.monitor.online.OnlineSession;
import com.frame.project.domain.monitor.online.UserOnline;
import com.frame.project.service.monitor.online.UserOnlineService;

/**
 * 
 * @className ：UserOnlineController
 * @describe ：在线用户监控
 */
@Controller
@RequestMapping("/monitor/online")
public class UserOnlineController extends BaseController
{
    private String prefix = "monitor/online";

    @Autowired
    private UserOnlineService userOnlineService;

    @Autowired
    private OnlineSessionDAO onlineSessionDAO;

    @RequiresPermissions("monitor:online:view")
    @GetMapping()
    public String online()
    {
        return prefix + "/online";
    }

    @RequiresPermissions("monitor:online:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserOnline userOnline,@RequestParam(defaultValue = "0") Integer pageNum, @RequestParam(defaultValue = "10") Integer pageSize)
    {
        startPage();
        Page<UserOnline> findByPage = userOnlineService.findByPage(pageNum, pageSize, userOnline);
        return getDataTable(findByPage);
    }

    @RequiresPermissions("monitor:online:batchForceLogout")
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @PostMapping("/batchForceLogout")
    @ResponseBody
    public WebError batchForceLogout(@RequestParam("ids[]") Long[] ids)
    {
        for (Long id : ids)
        {
            UserOnline online = userOnlineService.findById(id);
            if (online == null)
            {
                return error(-1,"用户已下线");
            }
            OnlineSession onlineSession = (OnlineSession) onlineSessionDAO.readSession(online.getSessionId());
            if (onlineSession == null)
            {
                return error(-1,"用户已下线");
            }
            String sessionId = online.getSessionId();
            if (sessionId.equals(ShiroUtils.getSessionId()))
            {
                return error(-1,"当前登陆用户无法强退");
            }
            onlineSession.setStatus(OnlineEnum.OFF_LINE.getValue());
            onlineSessionDAO.update(onlineSession);
            online.setStatus(OnlineEnum.OFF_LINE.getValue());
            userOnlineService.saveOnline(online);
        }
        return success();
    }

    @SuppressWarnings("unused")
	@RequiresPermissions("monitor:online:forceLogout")
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @PostMapping("/forceLogout")
    @ResponseBody
    public WebError forceLogout(Long id)
    {
        UserOnline online = userOnlineService.findById(id);
        String sessionId = online.getSessionId();
        if (sessionId.equals(ShiroUtils.getSessionId()))
        {
            return error(-1,"当前登陆用户无法强退");
        }
        if (online == null)
        {
            return error(-1,"用户已下线");
        }
        OnlineSession onlineSession = (OnlineSession) onlineSessionDAO.readSession(sessionId);
        if (onlineSession == null)
        {
            return error(-1,"用户已下线");
        }
        onlineSession.setStatus(OnlineEnum.OFF_LINE.getValue());
        onlineSessionDAO.update(onlineSession);
        online.setStatus(OnlineEnum.OFF_LINE.getValue());
        userOnlineService.saveOnline(online);
        return success();
    }
}
