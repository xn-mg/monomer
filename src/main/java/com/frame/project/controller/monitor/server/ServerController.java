package com.frame.project.controller.monitor.server;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.frame.project.domain.monitor.server.Server;

/**
 * 
 * @className ：ServerController
 * @describe ：服务器监控
 */
@Controller
@RequestMapping("/monitor/server")
public class ServerController
{
    private String prefix = "monitor/server";

    @GetMapping()
    public String server(Model model) throws Exception
    {
        Server server = new Server();
        server.copyTo();
        model.addAttribute("server", server);
        return prefix + "/server";
    }
}
