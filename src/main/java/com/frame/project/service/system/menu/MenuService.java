package com.frame.project.service.system.menu;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import com.frame.framework.web.domain.Ztree;
import com.frame.framework.web.service.BaseService;
import com.frame.project.domain.system.menu.Menu;
import com.frame.project.domain.system.role.Role;
import com.frame.project.domain.system.user.User;

/**
 * 
 * @className ：MenuService
 * @describe ：菜单
 */
public interface MenuService extends BaseService<Menu>{

	/**
     * 根据用户查询菜单
     * 
     * @param user 用户信息
     * @return 菜单列表
     */
	List<Menu> selectMenusByUser(User user);
	
	/**
     * 查询菜单集合
     * 
     * @return 所有菜单信息
     */
	List<Menu> selectMenuList(Menu menu);
	
	/**
     * 查询菜单集合
     * 
     * @return 所有菜单信息
     */
	List<Menu> selectMenuAll();
	
	/**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
	Set<String> selectPermsByUserId(Long userId);
	
	 /**
     * 根据角色ID查询菜单
     * 
     * @param role 角色对象
     * @return 菜单列表
     */
	List<Ztree> roleMenuTreeData(Role role);
	
	/**
     * 查询所有菜单
     * 
     * @return 菜单列表
     */
	List<Ztree> menuTreeData();
	
	/**
     * 查询系统所有权限
     * 
     * @return 权限列表
     */
	LinkedHashMap<String, String> selectPermsAll();
	
	/**
     * 删除菜单管理信息
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
	void deleteMenuById(Long menuId);
	
	/**
     * 查询子菜单数量
     * 
     * @param parentId 菜单ID
     * @return 结果
     */
	Integer selectCountMenuByParentId(Long parentId);
	
	/**
     * 查询菜单使用数量
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
	Integer selectCountRoleMenuByMenuId(Long menuId);
	
	/**
     * 新增保存菜单信息
     * 
     * @param menu 菜单信息
     */
	void insertMenu(Menu menu);
	
	/**
     * 修改保存菜单信息
     * 
     * @param menu 菜单信息
     */
	void updateMenu(Menu menu);
	
	/**
     * 校验菜单名称是否唯一
     * 
     * @param menu 菜单信息
     * @return 结果
     */
	String checkMenuNameUnique(Menu menu);
}
