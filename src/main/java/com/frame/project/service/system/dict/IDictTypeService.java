package com.frame.project.service.system.dict;

import java.util.List;

import org.springframework.data.domain.Page;

import com.frame.framework.web.domain.Ztree;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.service.BaseService;
import com.frame.project.domain.system.dict.DictType;

/**
 * 
 * @className ：DictTypeService
 * @describe ：字典类型
 */
public interface IDictTypeService extends BaseService<DictType>{

	/**
	 * 分页
	 * @param pageNo
	 * @param pageSize
	 * @param dictType
	 * @return
	 */
	Page<DictType> findByPage(Integer pageNo, Integer pageSize, DictType dictType);
	
	/**
     * 根据所有字典类型
     * 
     * @return 字典类型集合信息
     */
	List<DictType> selectDictTypeAll();
	
	 /**
     * 根据字典类型查询信息
     * 
     * @param dictType 字典类型
     * @return 字典类型
     */
	DictType selectDictTypeByType(String dictType);
	
	 /**
     * 新增保存字典类型信息
     * 
     * @param dictType 字典类型信息
     */
	void insertDictType(DictType dictType);
	
	/**
     * 修改保存字典类型信息
     * 
     * @param dictType 字典类型信息
     * @return 结果
     */
	void updateDictType(DictType dictType);
	
	/**
     * 批量删除字典类型
     * 
     * @param ids 需要删除的数据
     * @return 结果
     */
	WebError deleteDictTypeByIds(String ids);
	
	/**
     * 校验字典类型称是否唯一
     * 
     * @param dict 字典类型
     * @return 结果
     */
	String checkDictTypeUnique(DictType dict);
	
	 /**
     * 查询字典类型树
     * 
     * @param dictType 字典类型
     * @return 所有字典类型
     */
	List<Ztree> selectDictTree(DictType dictType);
	
	void loadingDictCache();
	
	void clearDictCache();
	
	void resetDictCache();
}
