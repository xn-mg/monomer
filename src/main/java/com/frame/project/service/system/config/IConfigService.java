package com.frame.project.service.system.config;

import org.springframework.data.domain.Page;

import com.frame.framework.web.service.BaseService;
import com.frame.project.domain.system.config.Config;

/**
 * 
 * @className ：ConfigService
 * @describe ：参数配置
 */
public interface IConfigService extends BaseService<Config>{

	/**
	  * 分页
	 */
	Page<Config> findByPage(Integer pageNo, Integer pageSize, Config config);
	
	/**
          * 新增参数配置
     * 
     * @param config 参数配置信息
     */
	void insertConfig(Config config);
	
	/**
	  * 修改参数配置
	 * 
	 * @param config 参数配置信息
	 */
	void updateConfig(Config config);
	
	/**
	 * 根据ID删除
	 * @param id
	 */
	void deleteConfigById(Long id);
	
	/**
          * 批量删除参数配置对象
     * 
     * @param ids 需要删除的数据ID
     */
	void deleteConfigByIds(String ids);
	
	/**
	  * 清空缓存数据
	*/
	void clearCache();
	
	 /**
          * 根据键名查询参数配置信息
     * 
     * @param configKey 参数名称
     * @return 参数键值
     */
	String selectConfigByKey(String configKey);
	
	 /**
     * 校验参数键名是否唯一
     * 
     * @param config 参数配置信息
     * @return 结果
     */
	String checkConfigKeyUnique(Config config);
}
