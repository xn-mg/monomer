package com.frame.project.service.system.dict;

import java.util.List;

import org.springframework.data.domain.Page;

import com.frame.framework.web.service.BaseService;
import com.frame.project.domain.system.dict.DictData;

/**
 * 
 * @className ：DictDataService
 * @describe ：数据字典
 */
public interface IDictDataService extends BaseService<DictData>{

	/**
	 * 分页
	 * @param pageNo
	 * @param pageSize
	 * @param dictData
	 * @return
	 */
	Page<DictData> findByPage(Integer pageNo, Integer pageSize, DictData dictData);
	
	/**
          * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
	String selectDictLabel(String dictType, String dictValue);
	
	/**
     * 根据字典类型查询字典数据
     * 
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
	List<DictData> selectDictDataByType(String dictType);
	
	/**
     * 新增保存字典数据信息
     * 
     * @param dictData 字典数据信息
     */
	void insertDictData(DictData dictData);
	
	/**
     * 修改保存字典数据信息
     * 
     * @param dictData 字典数据信息
     */
	void updateDictData(DictData dictData);
	
	 /**
     * 批量删除字典数据
     * 
     * @param ids 需要删除的数据
     */
	void deleteDictDataByIds(String ids);
}
