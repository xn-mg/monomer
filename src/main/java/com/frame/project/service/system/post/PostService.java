package com.frame.project.service.system.post;

import java.util.List;

import org.springframework.data.domain.Page;

import com.frame.framework.web.error.WebError;
import com.frame.framework.web.service.BaseService;
import com.frame.project.domain.system.post.Post;

/**
 * 
 * @className ：PostService
 * @describe ：岗位
 */
public interface PostService extends BaseService<Post>{

	/**
	 * 分页
	 * @param pageNo
	 * @param pageSize
	 * @param post
	 * @return
	 */
	Page<Post> findByPage(Integer pageNo, Integer pageSize, Post post);
	
	/**
     * 查询所有岗位
     * 
     * @return 岗位列表
     */
	List<Post> selectPostAll();
	
	/**
     * 根据用户ID查询岗位
     * 
     * @param userId 用户ID
     * @return 岗位列表
     */
	List<Post> selectPostsByUserId(Long userId);
	
	/**
     * 批量删除岗位信息
     * 
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
	WebError deletePostByIds(String ids);
	
	/**
     * 通过岗位ID查询岗位使用数量
     * 
     * @param postId 岗位ID
     * @return 结果
     */
	Integer countUserPostById(Long postId);
	
	/**
     * 校验岗位名称是否唯一
     * 
     * @param post 岗位信息
     * @return 结果
     */
	String checkPostNameUnique(Post post);
	
	/**
     * 校验岗位编码是否唯一
     * 
     * @param post 岗位信息
     * @return 结果
     */
	String checkPostCodeUnique(Post post);
	
	/**
	 * 修改
	 * @param post
	 */
	void updatePost(Post post);
}
