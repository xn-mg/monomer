package com.frame.project.service.system.role;

import java.util.List;

import com.frame.project.domain.system.role.RoleMenu;

/**
 * 
 * @className ：RoleMenuService
 * @describe ：角色和菜单
 */
public interface RoleMenuService {

	/**
     * 通过角色ID删除角色和菜单关联
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    void deleteRoleMenuByRoleId(Long roleId);
    
    /**
     * 批量删除角色菜单关联信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    void deleteRoleMenu(Long[] ids);
    
    /**
     * 查询菜单使用数量
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    Integer selectCountRoleMenuByMenuId(Long menuId);
    
    /**
     * 批量新增角色菜单信息
     * 
     * @param roleMenuList 角色菜单列表
     * @return 结果
     */
    void batchRoleMenu(List<RoleMenu> roleMenuList);
}
