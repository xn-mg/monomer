package com.frame.project.service.system.user.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.frame.project.domain.system.user.UserRole;
import com.frame.project.repository.system.user.UserRoleRepository;
import com.frame.project.service.system.user.UserRoleService;

/**
 * 
 * @className ：UserRoleServiceImpl
 * @describe ：用户和角色
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleRepository userRoleRepository;

	/**
     * 通过用户ID查询用户和角色关联
     * 
     * @param userId 用户ID
     * @return 用户和角色关联列表
     */
	@Override
	public List<UserRole> selectUserRoleByUserId(Long userId) {
		
		return userRoleRepository.findByUser_Id(userId);
	}

	/**
     * 通过用户ID删除用户和角色关联
     * 
     * @param userId 用户ID
     * @return 结果
     */
	@Override
	public void deleteUserRoleByUserId(Long userId) {
		
		userRoleRepository.deleteUserRoleByUserId(userId);
	}

	/**
     * 批量删除用户和角色关联
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public void deleteUserRole(Long[] ids) {
		for(Long id : ids) {
			userRoleRepository.deleteUserRoleByUserId(id);
		}
	}

	/**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
	@Override
	public Integer countUserRoleByRoleId(Long roleId) {
		
		return userRoleRepository.countUserRoleByRoleId(roleId);
	}

	/**
     * 批量新增用户角色信息
     * 
     * @param userRoleList 用户角色列表
     * @return 结果
     */
	@Override
	public void batchUserRole(List<UserRole> userRoleList) {
		for(UserRole ur : userRoleList) {
			userRoleRepository.save(ur);
		}
	}

	/**
     * 删除用户和角色关联信息
     * 
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
	@Override
	public void deleteUserRoleInfo(UserRole userRole) {
		
		userRoleRepository.deleteUserRolebyUserIdAndRoleId(userRole.getUser().getId(), userRole.getRole().getId());
	}

	/**
     * 批量取消授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
	@Override
	public void deleteUserRoleInfos(Long roleId, Long[] userIds) {
		
		List<UserRole> urList = userRoleRepository.findByRole_Id(roleId);
		
		for(Long uId : userIds) {
			for(UserRole ur : urList) {
				if(uId == ur.getUser().getId()) {
					userRoleRepository.deleteUserRoleByUserId(uId);
				}
			}
		}
	}
}
