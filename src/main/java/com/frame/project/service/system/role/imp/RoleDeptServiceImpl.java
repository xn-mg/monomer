package com.frame.project.service.system.role.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.frame.project.domain.system.role.RoleDept;
import com.frame.project.repository.system.role.RoleDeptRepository;
import com.frame.project.service.system.role.RoleDeptService;

/**
 * 
 * @className ：RoleDeptServiceImpl
 * @describe ：角色和部门
 */
@Service
public class RoleDeptServiceImpl implements RoleDeptService{

	@Autowired
	private RoleDeptRepository roleDeptRepository;

	/**
          * 通过角色ID删除角色和部门关联
     * 
     * @param roleId 角色ID
     * @return 结果
     */
	@Override
	public void deleteRoleDeptByRoleId(Long roleId) {
		roleDeptRepository.deleteRoleDeptByRoleId(roleId);
		
	}

	 /**
           * 批量删除角色部门关联信息
     * 
     * @param ids 需要删除的数据ID（roleId）
     * @return 结果
     */
	@Override
	public void deleteRoleDept(Long[] ids) {
		for(Long id : ids) {
			roleDeptRepository.deleteRoleDeptByRoleId(id);
		}
		
	}

	/**
          * 查询部门使用数量
     * 
     * @param deptId 部门ID
     * @return 结果
     */
	@Override
	public Integer selectCountRoleDeptByDeptId(Long deptId) {
		
		return roleDeptRepository.selectCountRoleDeptByDeptId(deptId);
	}

	/**
          * 批量新增角色部门信息
     * 
     * @param roleDeptList 角色部门列表
     * @return 结果
     */
	@Override
	public void batchRoleDept(List<RoleDept> roleDeptList) {
		for(RoleDept rd : roleDeptList ) {
			roleDeptRepository.save(rd);
		}
	}
}
