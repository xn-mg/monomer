package com.frame.project.service.system.dict.imp;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.frame.common.enumutil.RemoveEnum;
import com.frame.common.utils.StringUtils;
import com.frame.common.utils.text.Convert;
import com.frame.framework.utils.dict.DictUtils;
import com.frame.framework.web.service.imp.AbstractBaseService;
import com.frame.project.domain.system.dict.DictData;
import com.frame.project.repository.system.dict.DictDataRepository;
import com.frame.project.service.system.dict.IDictDataService;

/**
 * 
 * @className ：DictDataServiceImpl
 * @describe ：数据字典
 */
@Service
public class IDictDataServiceImpl extends AbstractBaseService<DictData> implements IDictDataService {

	@Autowired
	private DictDataRepository dictDataRepository;
	
	/**
	 * 分页
	*/
	@Override
	public Page<DictData> findByPage(Integer pageNo, Integer pageSize, DictData dictData) {
		List<Sort.Order> orders = new ArrayList<>();
		orders.add(new Sort.Order(Sort.Direction.ASC, "dictSort"));
		Sort sort = Sort.by(orders);
		Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
		
		Page<DictData> page = dictDataRepository.findAll((root, query, cb) ->{
			List<Predicate> list = new ArrayList<>();
			list.add(cb.and(cb.equal(root.get("removeStatus").as(Integer.class), RemoveEnum.NOT_REMOVE.getCode())));
			
			if(StringUtils.isNotBlank(dictData.getDictType())) {
				list.add(cb.and(cb.equal(root.get("dictType").as(String.class), dictData.getDictType())));
			}
			
			if(StringUtils.isNotBlank(dictData.getDictLabel())) {
				list.add(cb.and(cb.like(root.get("dictLabel").as(String.class), dictData.getDictLabel())));
			}
			
			if(StringUtils.isNotBlank(dictData.getStatus())) {
				list.add(cb.and(cb.equal(root.get("status").as(String.class), dictData.getStatus())));
			}
			
			Predicate[] p = new Predicate[list.size()];
			return cb.and(list.toArray(p));
		},pageable);
		
		return page;
	}
	
	/**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    public String selectDictLabel(String dictType, String dictValue)
    {
    	DictData dd = dictDataRepository.findByDictTypeAndDictValue(dictType, dictValue);
    	
        return dd.getDictLabel();
    }
    
    /**
     * 根据字典类型查询字典数据
     * 
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    @Override
    public List<DictData> selectDictDataByType(String dictType)
    {
        List<DictData> dictDatas = DictUtils.getDictCache(dictType);
        if (StringUtils.isNotNull(dictDatas))
        {
            return dictDatas;
        }
        dictDatas = dictDataRepository.findDictDataByType(dictType);
        if (StringUtils.isNotNull(dictDatas))
        {
            DictUtils.setDictCache(dictType, dictDatas);
            return dictDatas;
        }
        return null;
    }
    
    /**
          * 新增保存字典数据信息
     * 
     * @param dictData 字典数据信息
     */
    @Override
    public void insertDictData(DictData dictData)
    {
    	add(dictData);
    	
    	List<DictData> dictDatas = dictDataRepository.findDictDataByType(dictData.getDictType());
        DictUtils.setDictCache(dictData.getDictType(), dictDatas);
    }
    
    /**
     * 修改保存字典数据信息
     * 
     * @param dictData 字典数据信息
     * @return 结果
     */
    @Override
    public void updateDictData(DictData dictData)
    {
    	DictData data = findById(dictData.getId());
    	data.setDictLabel(dictData.getDictLabel());
    	data.setDictValue(dictData.getDictValue());
    	data.setCssClass(dictData.getCssClass());
    	data.setDictSort(dictData.getDictSort());
    	data.setListClass(dictData.getListClass());
    	data.setIsDefault(dictData.getIsDefault());
    	data.setStatus(dictData.getStatus());
    	data.setRemark(dictData.getRemark());
    	modify(data);
    	
    	List<DictData> dictDatas = dictDataRepository.findDictDataByType(dictData.getDictType());
        DictUtils.setDictCache(dictData.getDictType(), dictDatas);
    }
    
    /**
     * 批量删除字典数据
     * 
     * @param ids 需要删除的数据
     */
    @Override
    public void deleteDictDataByIds(String ids)
    {
        Long[] strArray = Convert.toLongArray(ids);
        for(Long id : strArray) {
        	DictData data = findById(id);
        	delete(id);
        	List<DictData> dictDatas = dictDataRepository.findDictDataByType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
    }
}
