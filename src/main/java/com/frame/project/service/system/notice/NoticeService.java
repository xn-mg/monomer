package com.frame.project.service.system.notice;

import org.springframework.data.domain.Page;

import com.frame.framework.web.service.BaseService;
import com.frame.project.domain.system.notice.Notice;

/**
 * 
 * @className ：NoticeService
 * @describe ：通知公告
 */
public interface NoticeService extends BaseService<Notice>{

	/**
	 * 分页
	 * @param pageNo
	 * @param pageSize
	 * @param notice
	 * @return
	 */
	Page<Notice> findByPage(Integer pageNo, Integer pageSize, Notice notice);
	
	/**
	 * 修改
	 * @param notice
	 */
	void updateNotice(Notice notice);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	void deleteNoticeByIds(String ids);
}
