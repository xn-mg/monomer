package com.frame.project.service.system.role;

import java.util.List;

import com.frame.project.domain.system.role.RoleDept;

/**
 * 
 * @className ：RoleDeptService
 * @describe ：角色和部门
 */
public interface RoleDeptService{

	/**
	  * 通过角色ID删除角色和部门关联
     * 
     * @param roleId 角色ID
     * @return 结果
     */
	void deleteRoleDeptByRoleId(Long roleId);
	
	/**
          * 批量删除角色部门关联信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	void deleteRoleDept(Long[] ids);
	
	/**
 	* 查询部门使用数量
	* 
	* @param deptId 部门ID
	* @return 结果
	*/
	Integer selectCountRoleDeptByDeptId(Long deptId);
	
	/**
          * 批量新增角色部门信息
     * 
     * @param roleDeptList 角色部门列表
     * @return 结果
     */
	void batchRoleDept(List<RoleDept> roleDeptList);
}
