package com.frame.project.service.system.role.imp;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.frame.common.constant.UserConstants;
import com.frame.common.enumutil.RemoveEnum;
import com.frame.common.exception.ServiceException;
import com.frame.common.utils.StringUtils;
import com.frame.common.utils.page.MyPage;
import com.frame.common.utils.security.ShiroUtils;
import com.frame.common.utils.spring.SpringUtils;
import com.frame.common.utils.text.Convert;
import com.frame.framework.aspectj.DataScopeAspect;
import com.frame.framework.aspectj.lang.annotation.DataScope;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.service.imp.AbstractBaseService;
import com.frame.project.domain.system.role.Role;
import com.frame.project.domain.system.role.RoleDept;
import com.frame.project.domain.system.role.RoleMenu;
import com.frame.project.domain.system.user.User;
import com.frame.project.domain.system.user.UserRole;
import com.frame.project.repository.system.role.RoleRepository;
import com.frame.project.service.system.dept.DeptService;
import com.frame.project.service.system.menu.MenuService;
import com.frame.project.service.system.role.RoleDeptService;
import com.frame.project.service.system.role.RoleMenuService;
import com.frame.project.service.system.role.RoleService;
import com.frame.project.service.system.user.UserRoleService;
import com.frame.project.service.system.user.UserService;

/**
 * 
 * @className ：RoleServiceImpl
 * @describe ：角色
 */
@Service
public class RoleServiceImpl extends AbstractBaseService<Role> implements RoleService{

	@Autowired 
	private RoleRepository roleRepository;
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private DeptService deptService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleMenuService roleMenuService;
	
	@Autowired
	private UserRoleService userRoleService;
	
	@Autowired
	private RoleDeptService roleDeptService;
	
	@PersistenceContext
	private EntityManager em;
	
	/**
	 * 分页
	 */
	@Override
	@DataScope(deptAlias = "d")
	public MyPage<Role> findByPage(Role role, Integer pageNo, Integer pageSize) {
		StringBuilder sql = new StringBuilder(
                "select distinct r.id, r.role_name, r.role_key, r.order_num, r.data_scope, r.status, r.remove_status, r.create_time" +
                        " from sys_role r" +
                        " left join sys_user_role ur on ur.role_id = r.id" +
                        " left join sys_user u on u.id = ur.user_id" +
                        " left join sys_dept d on u.dept_id = d.id" +
                        " where r.remove_status = 0");

        if(role.getUserId() != null){
            sql.append(" and u.id = ").append(role.getUserId());
        }

        if(StringUtils.isNotBlank(role.getRoleName())){
            sql.append(" and r.role_name like concat('%', '").append(role.getRoleName()).append("', '%')");
        }

        if(StringUtils.isNotBlank(role.getStatus())){
            sql.append(" and r.status = '").append(role.getStatus()).append("'");
        }

        if(StringUtils.isNotBlank(role.getRoleKey())){
            sql.append(" and r.role_key like concat('%', '").append(role.getRoleKey()).append("', '%')");
        }

        if(role.getParams() != null){
            Map<String, Object> params = role.getParams();
            String beginTime = (String) params.get("beginTime");
            String endTime = (String) params.get("endTime");
            String sqlString = (String)params.get(DataScopeAspect.DATA_SCOPE);

            if(StringUtils.isNotBlank(beginTime)){
                sql.append(" and date_format(r.create_time,'%y%m%d') >= date_format('").append(beginTime).append("','%y%m%d')");
            }
            if(StringUtils.isNotBlank(endTime)){
                sql.append(" and date_format(r.create_time,'%y%m%d') <= date_format('").append(endTime).append("','%y%m%d')");
            }
            if(StringUtils.isNotBlank(sqlString)){
                sql.append(sqlString);
            }
        }
        Query query = em.createNativeQuery(sql.toString());
        int firstIndex = pageNo * pageSize;
        query.setFirstResult(firstIndex);
        query.setMaxResults(pageSize);
        List<Object[]> list = query.getResultList();
        String countSql = "select count(*) from ("+sql.toString()+") c";
        Query countQuery = em.createNativeQuery(countSql);
        Object obj = countQuery.getSingleResult();
        Integer tatal = Integer.parseInt(obj.toString());
        BigDecimal bd = BigDecimal.valueOf(tatal);
        BigDecimal bdsize = BigDecimal.valueOf(pageSize);
        BigDecimal bdvi = bd.divide(bdsize, 1, BigDecimal.ROUND_HALF_UP);
        Integer totalPage = (int)Math.ceil(bdvi.doubleValue());
        List<Role> roleList = new ArrayList<Role>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        for(Object[] objects : list){
        	Role r = new Role();
            r.setId(Long.parseLong(objects[0].toString()));
            r.setRoleName(objects[1].toString());
            r.setRoleKey(objects[2].toString());
            r.setOrderNum(Integer.parseInt(objects[3].toString()));
            if(objects[4] != null) {
            	r.setDataScope(objects[4].toString());
            }
            r.setStatus(objects[5].toString());
            r.setRemoveStatus(Integer.parseInt(objects[6].toString()));
            try {
                r.setCreateTime(format.parse(objects[7].toString()));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            roleList.add(r);
        }

        MyPage<Role> page = new MyPage<Role>(pageNo,totalPage,roleList,new  com.frame.common.utils.page.Pageable(pageSize),tatal+"",tatal);

        return page;
	}
	
	/**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectRoleKeys(Long userId)
    {
    	String sql = "WHERE r.remove_status = '0' and ur.user_id = "+userId;
        List<Role> perms = selectRoleContactVo(sql);
        Set<String> permsSet = new HashSet<>();
        for (Role perm : perms)
        {
            if (StringUtils.isNotNull(perm))
            {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }
    
    /**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 角色列表
     */
    @Override
    public List<Role> selectRolesByUserId(Long userId)
    {
    	String sql = "WHERE r.remove_status = '0' and ur.user_id = "+userId;
        List<Role> userRoles = selectRoleContactVo(sql);
        List<Role> roles = selectRoleAll();
        
        for (Role role : roles)
        {
            for (Role userRole : userRoles)
            {
                if (role.getId() == userRole.getId())
                {
                    role.setFlag(true);
                    break;
                }
            }
        }
        return roles;
    }
    
    /**
     * 查询所有角色
     * 
     * @return 角色列表
     */
    @Override
    public List<Role> selectRoleAll()
    {
        return roleRepository.findAll().stream().filter(role -> role.getRemoveStatus() == RemoveEnum.NOT_REMOVE.getCode()).collect(Collectors.toList());
    }
    
    /**
     * 新增保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public void insertRole(Role role)
    {
    	role.setCreateBy(ShiroUtils.getLoginName());
    	role.setCreateTime(new Date());
    	role.setUpdateTime(new Date());

        // 新增角色信息
    	add(role);
        insertRoleMenu(role);
    }
    
    /**
     * 修改保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public void updateRole(Role role)
    {
    	Role r = findById(role.getId());
    	r.setRoleName(role.getRoleName());
    	r.setRoleKey(role.getRoleKey());
    	r.setOrderNum(role.getOrderNum());
    	r.setStatus(role.getStatus());
    	r.setRemark(role.getRemark());
        // 修改角色信息
    	modify(r);
        // 删除角色与菜单关联
        roleMenuService.deleteRoleMenuByRoleId(role.getId());
        insertRoleMenu(role);
    }
    
    /**
     * 修改数据权限信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public void authDataScope(Role role)
    {
    	Role r = findById(role.getId());
    	r.setDataScope(role.getDataScope());
        // 修改角色信息
    	modify(r);
        // 删除角色与部门关联
    	roleDeptService.deleteRoleDeptByRoleId(role.getId());
        // 新增角色和部门信息（数据权限）
        insertRoleDept(role);
    }
    
    /**
     * 批量删除角色信息
     * 
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
    @Override
    public WebError deleteRoleByIds(String ids)
    {
        Long[] roleIds = Convert.toLongArray(ids);
        for (Long roleId : roleIds)
        {
            WebError web = checkRoleAllowed(roleId);
            if(web.getCode() == -1) {
            	return web;
            }
            Role role = findById(roleId);
            if (countUserRoleByRoleId(roleId) > 0)
            {
                return new WebError(-1, role.getRoleName() + "已分配,不能删除");
            }
        }
        
        for(Long id : roleIds) {
        	roleRepository.delete(findById(id));
        }
        
        return new WebError();
    }
    
    /**
     * 校验角色是否允许操作
     * 
     * @param role 角色信息
     */
    @Override
    public WebError checkRoleAllowed(Long roleId)
    {
    	Role role = findById(roleId);
        if (StringUtils.isNotNull(roleId) && role.isAdmin())
        {
            return new WebError(-1, "不允许操作超级管理员角色");
        }
        
        return new WebError();
    }
    
    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public Integer countUserRoleByRoleId(Long roleId)
    {
        return userRoleService.countUserRoleByRoleId(roleId);
    }
    
    /**
     * 校验角色是否有数据权限
     * 
     * @param roleId 角色id
     */
    @Override
    public void checkRoleDataScope(Long roleId)
    {
    	User u = userService.findById(ShiroUtils.getUserId());
        if (!User.isAdmin(u.getLoginName()))
        {
            Role role = new Role();
            role.setId(roleId);
            List<Role> roles = SpringUtils.getAopProxy(this).findByPage(role,0,Integer.MAX_VALUE).getContent();
            if (StringUtils.isEmpty(roles))
            {
                throw new ServiceException("没有权限访问角色数据！");
            }
        }
    }
    
    /**
     * 新增角色菜单信息
     * 
     * @param role 角色对象
     */
    public void insertRoleMenu(Role role)
    {
        // 新增用户与角色管理
        List<RoleMenu> list = new ArrayList<RoleMenu>();
        for (Long menuId : role.getMenuIds())
        {
            RoleMenu rm = new RoleMenu();
            rm.setRole(findById(role.getId()));
            rm.setMenu(menuService.findById(menuId));
            list.add(rm);
        }
        if (list.size() > 0)
        {
            roleMenuService.batchRoleMenu(list);
        }
    }
    
    /**
     * 新增角色部门信息(数据权限)
     *
     * @param role 角色对象
     */
    public void insertRoleDept(Role role)
    {
        // 新增角色与部门（数据权限）管理
        List<RoleDept> list = new ArrayList<RoleDept>();
        for (Long deptId : role.getDeptIds())
        {
            RoleDept rd = new RoleDept();
            rd.setRole(findById(role.getId()));
            rd.setDept(deptService.findById(deptId));
            list.add(rd);
        }
        if (list.size() > 0)
        {
        	roleDeptService.batchRoleDept(list);
        }
    }
    
    public List<Role> selectRoleContactVo(String sqlString){
    	StringBuilder sql = new StringBuilder(
				"select distinct r.id, r.role_name, r.role_key, r.order_num, r.data_scope,r.status, r.remove_status, r.create_time, r.remark" + 
				" from sys_role r left join sys_user_role ur on ur.role_id = r.id" + 
				" left join sys_user u on u.id = ur.user_id" + 
				" left join sys_dept d on u.dept_id = d.id ");
    	
    	if(StringUtils.isNotBlank(sqlString)) {
    		sql.append(sqlString);
    	}
    	Query query = em.createNativeQuery(sql.toString());
    	List<Object[]> list = query.getResultList();
    	List<Role> RoleList = new ArrayList<Role>();
    	
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    	
    	for(Object[] objects : list) {
    		Role role = new Role();
    		
    		role.setId(Long.parseLong(objects[0].toString()));
    		role.setRoleName(objects[1].toString());
    		role.setRoleKey(objects[2].toString());
    		role.setOrderNum(Integer.valueOf(objects[3].toString()));
    		if(StringUtils.isNotNull(objects[4])) {
    			role.setDataScope(objects[4].toString());
    		}else {
    			role.setDataScope(null);
    		}
    		role.setStatus(objects[5].toString());
    		role.setRemoveStatus(Integer.parseInt(objects[6].toString()));
    		try {
				role.setCreateTime(format.parse(objects[7].toString()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		role.setRemark(objects[8].toString());
    		
    		RoleList.add(role);
	    }
    	
    	return RoleList;
    }
    
    /**
     * 校验角色名称是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleNameUnique(Role role)
    {
        Long roleId = StringUtils.isNull(role.getId()) ? -1L : role.getId();
        Role info = roleRepository.findByRoleName(role.getRoleName());
        
        if (StringUtils.isNotNull(info) && info.getId() != roleId)
        {
            return UserConstants.ROLE_NAME_NOT_UNIQUE;
        }
        return UserConstants.ROLE_NAME_UNIQUE;
    }
    
    /**
     * 校验角色权限是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleKeyUnique(Role role)
    {
        Long roleId = StringUtils.isNull(role.getId()) ? -1L : role.getId();
        Role info = roleRepository.findByRoleKey(role.getRoleKey());
        if (StringUtils.isNotNull(info) && info.getId() != roleId)
        {
            return UserConstants.ROLE_KEY_NOT_UNIQUE;
        }
        return UserConstants.ROLE_KEY_UNIQUE;
    }
    
    /**
     * 角色状态修改
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public void changeStatus(Role role)
    {
    	
    	Role r = findById(role.getId());
    	r.setStatus(role.getStatus());
    	modify(r);
    }
    
    /**
     * 取消授权用户角色
     * 
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    @Override
    public void deleteAuthUser(UserRole userRole)
    {
        userRoleService.deleteUserRoleInfo(userRole);
    }
    
    /**
     * 批量取消授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    @Override
    public void deleteAuthUsers(Long roleId, String userIds)
    {
    	userRoleService.deleteUserRoleInfos(roleId, Convert.toLongArray(userIds));
    }
    
    /**
     * 批量选择授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要授权的用户数据ID
     * @return 结果
     */
    @Override
    public void insertAuthUsers(Long roleId, String userIds)
    {
        Long[] users = Convert.toLongArray(userIds);
        // 新增用户与角色管理
        List<UserRole> list = new ArrayList<UserRole>();
        for (Long userId : users)
        {
            UserRole ur = new UserRole();
            ur.setUser(userService.findById(userId));
            ur.setRole(findById(roleId));
            list.add(ur);
        }
        userRoleService.batchUserRole(list);
    }
}
