package com.frame.project.service.system.dept;

import java.util.List;

import com.frame.framework.web.domain.Ztree;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.service.BaseService;
import com.frame.project.domain.system.dept.Dept;
import com.frame.project.domain.system.role.Role;

/**
 * 
 * @className ：DeptService
 * @describe ：部门
 */
public interface DeptService extends BaseService<Dept>{

	/**
     * 查询部门管理数据
     * 
     * @param dept 部门信息
     * @return 部门信息集合
     */
	List<Dept> selectDeptList(Dept dept);
	
	/**
     * 查询部门管理树
     * 
     * @param dept 部门信息
     * @return 所有部门信息
     */
	List<Ztree> selectDeptTree(Dept dept);
	
	/**
     * 根据角色ID查询部门（数据权限）
     *
     * @param role 角色对象
     * @return 部门列表（数据权限）
     */
	List<Ztree> roleDeptTreeData(Role role);
	
	 /**
     * 查询部门人数
     * 
     * @param parentId 部门ID
     * @return 结果
     */
	Integer selectDeptCountByParent(Long parentId);
	
	/**
     * 查询部门是否存在用户
     * 
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
	boolean checkDeptExistUser(Long deptId);
	
	/**
     * 新增保存部门信息
     * 
     * @param dept 部门信息
     */
	WebError insertDept(Dept dept);
	
	 /**
     * 修改保存部门信息
     * 
     * @param dept 部门信息
     */
	void updateDept(Dept dept);
	
	/**
     * 根据ID查询所有子部门（正常状态）
     * 
     * @param deptId 部门ID
     * @return 子部门数
     */
	Integer selectNormalChildrenDeptById(Long deptId);
	
	 /**
     * 校验部门名称是否唯一
     * 
     * @param dept 部门信息
     * @return 结果
     */
	String checkDeptNameUnique(Dept dept);
	
	/**
	 * 根据父类ID查询
	 * @param parentId
	 * @return
	 */
	Dept findByParentId(Long parentId);
	
	/**
     * 校验部门是否有数据权限
     * 
     * @param deptId 部门id
     */
    public void checkDeptDataScope(Long deptId);
}
