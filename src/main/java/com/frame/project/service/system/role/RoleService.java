package com.frame.project.service.system.role;

import java.util.List;
import java.util.Set;

import com.frame.common.utils.page.MyPage;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.service.BaseService;
import com.frame.project.domain.system.role.Role;
import com.frame.project.domain.system.user.UserRole;

/**
 * 
 * @className ：RoleService
 * @describe ：角色
 */
public interface RoleService extends BaseService<Role>{

	/**
	  * 分页
	 * @param pageNo
	 * @param pageSize
	 * @param role
	 * @return
	 */
	MyPage<Role> findByPage(Role role, Integer pageNo, Integer pageSize);
	
	/**
          * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
	Set<String> selectRoleKeys(Long userId);
	
	/**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 角色列表
     */
	List<Role> selectRolesByUserId(Long userId);
	
	/**
     * 查询所有角色
     * 
     * @return 角色列表
     */
	List<Role> selectRoleAll();
	
	/**
     * 新增保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
	void insertRole(Role role);
	
	 /**
     * 修改保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    void updateRole(Role role);
    
    /**
     * 修改数据权限信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    void authDataScope(Role role);
    
	/**
     * 批量删除角色信息
     * 
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
	WebError deleteRoleByIds(String ids);
	
	/**
     * 校验角色是否允许操作
     * 
     * @param role 角色信息
     */
	WebError checkRoleAllowed(Long roleId);
	
	/**
     * 校验角色是否有数据权限
     * 
     * @param roleId 角色id
     */
    void checkRoleDataScope(Long roleId);
	
	/**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    Integer countUserRoleByRoleId(Long roleId);
    
    /**
     * 校验角色名称是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    String checkRoleNameUnique(Role role);
    
    /**
     * 校验角色权限是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    String checkRoleKeyUnique(Role role);
    
    /**
     * 角色状态修改
     * 
     * @param role 角色信息
     * @return 结果
     */
    void changeStatus(Role role);
    
    /**
     * 取消授权用户角色
     * 
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    void deleteAuthUser(UserRole userRole);
    
    /**
     * 批量取消授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    void deleteAuthUsers(Long roleId, String userIds);
    
    /**
     * 批量选择授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    void insertAuthUsers(Long roleId, String userIds);
}
