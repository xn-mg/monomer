package com.frame.project.service.system.role.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.frame.project.domain.system.role.RoleMenu;
import com.frame.project.repository.system.role.RoleMenuRepository;
import com.frame.project.service.system.role.RoleMenuService;

/**
 * 
 * @className ：RoleMenuServiceImpl
 * @describe ：角色和菜单
 */
@Service
public class RoleMenuServiceImpl implements RoleMenuService{

	@Autowired
	private RoleMenuRepository roleMenuRepository;

	/**
     * 通过角色ID删除角色和菜单关联
     * 
     * @param roleId 角色ID
     * @return 结果
     */
	@Override
	public void deleteRoleMenuByRoleId(Long roleId) {
		
		roleMenuRepository.deleteRoleMenuByRoleId(roleId);
	}

	/**
     * 批量删除角色菜单关联信息
     * 
     * @param ids 需要删除的数据ID（roleId）
     * @return 结果
     */
	@Override
	public void deleteRoleMenu(Long[] ids) {
		for(Long id : ids) {
			roleMenuRepository.deleteRoleMenuByRoleId(id);
		}
	}

	/**
     * 查询菜单使用数量
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
	@Override
	public Integer selectCountRoleMenuByMenuId(Long menuId) {
		
		return roleMenuRepository.selectCountRoleMenuByMenuId(menuId);
	}

	/**
     * 批量新增角色菜单信息
     * 
     * @param roleMenuList 角色菜单列表
     * @return 结果
     */
	@Override
	public void batchRoleMenu(List<RoleMenu> roleMenuList) {
		for(RoleMenu rm : roleMenuList) {
			roleMenuRepository.save(rm);
		}
	}
}
