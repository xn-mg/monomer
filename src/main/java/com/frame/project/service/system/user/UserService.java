package com.frame.project.service.system.user;

import java.util.List;
import com.frame.common.utils.page.MyPage;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.service.BaseService;
import com.frame.project.domain.system.user.User;
import com.frame.project.domain.system.user.UserRole;

/**
 * 
 * @className ：UserService
 * @describe ：用户
 */
public interface UserService extends BaseService<User>{

	/**
	 * 分页
	 * @param pageNo
	 * @param pageSize
	 * @param user
	 * @return
	 */
	MyPage<User> findByPage(User user, Integer pageNo, Integer pageSize);
	
	/**
     * 根据条件分页查询已分配用户角色列表
     * 
     * @param user 用户信息
     * @return 用户信息集合信息
     */
	MyPage<User> selectAllocatedList(User user, Integer pageNo, Integer pageSize);
	
	/**
     * 根据条件分页查询未分配用户角色列表
     * 
     * @param user 用户信息
     * @return 用户信息集合信息
     */
	MyPage<User> selectUnallocatedList(User user, Integer pageNo, Integer pageSize);
	
	/**
     * 通过用户名查询用户
     * 
     * @param userName 用户名
     * @return 用户对象信息
     */
    User selectUserByLoginName(String userName);
    
    /**
     * 通过手机号码查询用户
     * 
     * @param phoneNumber 手机号码
     * @return 用户对象信息
     */
    User selectUserByPhoneNumber(String phoneNumber);
    
    /**
     * 通过邮箱查询用户
     * 
     * @param email 邮箱
     * @return 用户对象信息
     */
    User selectUserByEmail(String email);
	
	/**
     * 通过用户ID查询用户和角色关联
     * 
     * @param userId 用户ID
     * @return 用户和角色关联列表
     */
    List<UserRole> selectUserRoleByUserId(Long userId);
    
    /**
     * 通过用户ID删除用户
     * 
     * @param userId 用户ID
     * @return 结果
     */
    void deleteUserById(Long userId);
    
    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    WebError deleteUserByIds(String ids);
    
    /**
     * 新增保存用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    void insertUser(User user);
    
    /**
     * 注册用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    boolean registerUser(User user);
    
    /**
     * 修改保存用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    void updateUser(User user);
    
    /**
     * 修改用户个人详细信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    boolean updateUserInfo(User user);
	
    /**
     * 用户授权角色
     * 
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    void insertUserAuth(Long userId, Long[] roleIds);
    
    /**
     * 校验登录名称是否唯一
     * 
     * @param loginName 用户名
     * @return
     */
    String checkLoginNameUnique(String loginName);
    
    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return
     */
    String checkPhoneUnique(User user);
    
    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return
     */
    String checkEmailUnique(User user);
    
    /**
     * 修改用户密码
     * 
     * @param user 用户信息
     * @return 结果
     */
    boolean resetUserPwd(User user);
    
    /**
     * 校验用户是否允许操作
     * 
     * @param user 用户信息
     */
    WebError checkUserAllowed(Long userId);
    
    /**
     * 校验用户是否有数据权限
     * 
     * @param userId 用户id
     */
    void checkUserDataScope(Long userId);
    
    /**
     * 查询用户所属角色组
     * 
     * @param userId 用户ID
     * @return 结果
     */
    String selectUserRoleGroup(Long userId);
    
    /**
     * 查询用户所属岗位组
     * 
     * @param userId 用户ID
     * @return 结果
     */
    String selectUserPostGroup(Long userId);
    
    /**
     * 导入用户数据
     * 
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @return 结果
     */
    String importUser(List<User> userList, Boolean isUpdateSupport);
    
    /**
     * 用户状态修改
     * 
     * @param user 用户信息
     * @return 结果
     */
    void changeStatus(User user);
}
