package com.frame.project.service.system.user;

import java.util.List;

import com.frame.project.domain.system.user.UserPost;

/**
 * 
 * @className ：UserPostService
 * @describe ：用户和岗位
 */
public interface UserPostService {

	/**
     * 通过用户ID删除用户和岗位关联
     * 
     * @param userId 用户ID
     * @return 结果
     */
    void deleteUserPostByUserId(Long userId);
    
    /**
     * 通过岗位ID查询岗位使用数量
     * 
     * @param postId 岗位ID
     * @return 结果
     */
    Integer countUserPostById(Long postId);
    
    /**
     * 批量删除用户和岗位关联
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    void deleteUserPost(Long[] ids);

    /**
     * 批量新增用户岗位信息
     * 
     * @param userPostList 用户角色列表
     * @return 结果
     */
    void batchUserPost(List<UserPost> userPostList);
}
