package com.frame.project.service.system.user.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.frame.project.domain.system.user.UserPost;
import com.frame.project.repository.system.user.UserPostRepository;
import com.frame.project.service.system.user.UserPostService;

/**
 * 
 * @className ：UserPostImpl
 * @describe ：用户和岗位
 */
@Service
public class UserPostServiceImpl implements UserPostService {
	
	@Autowired
	private UserPostRepository userPostRepository;

	/**
     * 通过用户ID删除用户和岗位关联
     * 
     * @param userId 用户ID
     * @return 结果
     */
	@Override
	public void deleteUserPostByUserId(Long userId) {
		
		userPostRepository.deleteUserPostByUserId(userId);
	}

	/**
     * 通过岗位ID查询岗位使用数量
     * 
     * @param postId 岗位ID
     * @return 结果
     */
	@Override
	public Integer countUserPostById(Long postId) {
		
		return userPostRepository.countUserPostById(postId);
	}

	/**
     * 批量删除用户和岗位关联
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public void deleteUserPost(Long[] ids) {
		for(Long id : ids) {
			userPostRepository.deleteUserPostByUserId(id);
		}
	}

	/**
     * 批量新增用户岗位信息
     * 
     * @param userPostList 用户角色列表
     * @return 结果
     */
	@Override
	public void batchUserPost(List<UserPost> userPostList) {
		for(UserPost up : userPostList) {
			userPostRepository.save(up);
		}
	}
}
