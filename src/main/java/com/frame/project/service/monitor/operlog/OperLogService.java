package com.frame.project.service.monitor.operlog;

import org.springframework.data.domain.Page;

import com.frame.project.domain.monitor.operlog.OperLog;

/**
 * 
 * @className ：OperLogService
 * @describe ：操作日志记录
 */
public interface OperLogService {

	/**
	 * 分页
	 * @param pageNo
	 * @param pageSize
	 * @param operLog
	 * @return
	 */
	Page<OperLog> findByPage(Integer pageNo, Integer pageSize, OperLog operLog);
	/**
     * 新增操作日志
     * 
     * @param operLog 操作日志对象
     */
	void insertOperlog(OperLog operLog);
	
	 /**
     * 通过操作日志ID查询操作日志
     * 
     * @param operLogId 操作日志ID
     * @return 操作日志对象信息
     */
	OperLog selectOperLogById(Long operLogId);
	
	/**
     * 删除操作日志
     * （物理删除）
     * @param jobId 操作日志ID
     */
	void deleteOperLogById(Long operLogId);
	
	/**
     * 批量删除操作日志
     * 
     * @param ids 需要删除的数据
     * @return
     */
	void deleteOperLogByIds(String ids);
	
	/**
     * 清空操作日志
     */
	void cleanOperLog();
}
