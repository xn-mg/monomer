package com.frame.project.service.monitor.online;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.frame.project.domain.monitor.online.UserOnline;

/**
 * 
 * @className ：UserOnlineService
 * @describe ：当前在线会话
 */
public interface UserOnlineService {

	/**
	 * 分页
	 * @param pageNo
	 * @param pageSize
	 * @param userOnline
	 * @return
	 */
	Page<UserOnline> findByPage(Integer pageNo, Integer pageSize, UserOnline userOnline);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	UserOnline findById(Long id);
	
	/**
	 * 根据sessionId查询
	 * @param sessionId
	 * @return
	 */
	UserOnline findBySessionId(String sessionId);
	
	/**
	 * 保存会话信息
	 * @param online
	 */
	void saveOnline(UserOnline online);
	
	/**
     * 通过会话序号查询信息
     * 
     * @param sessionId 会话ID
     * @return 在线用户信息
     */
	UserOnline selectOnlineBySessionId(String sessionId);
	
	/**
     * 通过会话序号删除信息
     * 
     * @param sessionId 会话ID
     * @return 在线用户信息
     */
	void deleteOnlineBySessionId(String sessionId);
	
	/**
     * 通过会话序号删除信息
     * 
     * @param sessions 会话ID集合
     * @return 在线用户信息
     */
	void batchDeleteOnline(List<String> sessions);
	
	/**
     * 强退用户
     * 
     * @param sessionId 会话ID
     */
	void forceLogout(String sessionId);
	
	/**
     * 清理用户缓存
     * 
     * @param loginName 登录名称
     * @param sessionId 会话ID
     */
	void removeUserCache(String loginName, String sessionId);
	
	/**
     * 根据最后访问时间
     * 查询会话集合
     * 
     * @param online 会话信息
     */
	List<UserOnline> selectOnlineByExpired(Date expiredDate);
}
