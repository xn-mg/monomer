package com.frame.project.service.monitor.job;

import org.springframework.data.domain.Page;

import com.frame.project.domain.monitor.job.JobLog;

/**
 * 
 * @className ：JobLogService
 * @describe ：定时任务调度信息日志
 */
public interface JobLogService {

	/**
	 * 分页
	 * @param pageNo
	 * @param pageSize
	 * @param jobLog
	 * @return
	 */
	Page<JobLog> findByPage(Integer pageNo, Integer pageSize, JobLog jobLog);
	
	/**
	 * 新增
	 * @param jobLog
	 */
	void addJobLog(JobLog jobLog);
	
	/**
	 * 通过ID查询
	 * @param jobLogId
	 * @return
	 */
	JobLog selectJobLogById(Long jobLogId);
	
	/**
     * 删除任务日志
     * 
     * @param JobLogId 调度日志ID
     */
	void deleteJobLogById(Long JobLogId);
	
	/**
     * 批量删除调度日志信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	void deleteJobLogByIds(String ids);
	
	 /**
     * 清空任务日志
     */
	void cleanJobLog();
}
