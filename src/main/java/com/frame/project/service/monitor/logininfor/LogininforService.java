package com.frame.project.service.monitor.logininfor;

import org.springframework.data.domain.Page;

import com.frame.project.domain.monitor.logininfor.Logininfor;

/**
 * 
 * @className ：LogininforService
 * @describe ：系统访问记录
 */
public interface LogininforService {

	/**
     * 分页
     */
	Page<Logininfor> findByPage(Integer pageNo, Integer pageSize, Logininfor logininfor);
	
	/**
     * 新增系统登录日志
     * 
     * @param logininfor 访问日志对象
     */
	void insertLogininfor(Logininfor logininfor);
	
	 /**
     * 通过系统访问记录ID查询系统访问记录
     * 
     * @param logininforId 系统访问记录ID
     * @return 系统访问记录对象信息
     */
	Logininfor selectLogininforById(Long logininforId);
	
	/**
     * 删除系统访问记录
     * （物理删除）
     * @param logininforId 系统访问记录ID
     */
	void deleteLogininforById(Long logininforId);
	
	/**
     * 批量删除系统登录日志
     * 
     * @param ids 需要删除的数据
     */
	void deleteLogininforByIds(String ids);
	
	/**
     * 清空系统登录日志
     */
	void cleanLogininfor();
}
