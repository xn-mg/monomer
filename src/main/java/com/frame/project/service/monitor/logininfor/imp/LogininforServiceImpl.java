package com.frame.project.service.monitor.logininfor.imp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.frame.common.enumutil.RemoveEnum;
import com.frame.common.utils.StringUtils;
import com.frame.common.utils.text.Convert;
import com.frame.project.domain.monitor.logininfor.Logininfor;
import com.frame.project.repository.monitor.logininfor.LogininforRepository;
import com.frame.project.service.monitor.logininfor.LogininforService;

/**
 * 
 * @className ：LogininforServiceImpl
 * @describe ：系统访问记录
 */
@Service
public class LogininforServiceImpl implements LogininforService {

	@Autowired
	private LogininforRepository logininforRepository;
	
	/**
     * 分页
     */
	@Override
	public Page<Logininfor> findByPage(Integer pageNo, Integer pageSize, Logininfor logininfor) {
		List<Sort.Order> orders = new ArrayList<>();
		orders.add(new Sort.Order(Sort.Direction.DESC, "id"));
		Sort sort = Sort.by(orders);
		Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Page<Logininfor> page = logininforRepository.findAll((root, query, cb) ->{
			List<Predicate> list = new ArrayList<>();
			list.add(cb.and(cb.equal(root.get("removeStatus").as(Integer.class), RemoveEnum.NOT_REMOVE.getCode())));
			
			if(StringUtils.isNotBlank(logininfor.getIpaddr())) {
				list.add(cb.and(cb.like(root.get("ipaddr").as(String.class), logininfor.getIpaddr())));
			}
			
			if(StringUtils.isNotBlank(logininfor.getStatus())) {
				list.add(cb.and(cb.equal(root.get("status").as(String.class), logininfor.getStatus())));
			}
			
			if(StringUtils.isNotBlank(logininfor.getLoginName())) {
				list.add(cb.and(cb.like(root.get("loginName").as(String.class), logininfor.getLoginName())));
			}
			
			Map<String, Object> params = logininfor.getParams();
			if(!params.isEmpty()) {
				String beginTime = (String) params.get("beginTime");
				String endTime = (String) params.get("endTime");
				if(StringUtils.isNotBlank(beginTime)) {
					try {
						list.add(cb.and(cb.greaterThanOrEqualTo(root.get("loginTime"), sdf.parse(beginTime))));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(StringUtils.isNotBlank(endTime)) {
					try {
						list.add(cb.and(cb.lessThanOrEqualTo(root.get("loginTime"), sdf.parse(endTime))));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			Predicate[] p = new Predicate[list.size()];
			return cb.and(list.toArray(p));
		},pageable);
		
		return page;
	}
	
	/**
     * 新增系统登录日志
     * 
     * @param logininfor 访问日志对象
     */
    @Override
    public void insertLogininfor(Logininfor logininfor)
    {
    	logininforRepository.save(logininfor);
    }
    
	/**
     * 通过调度任务日志ID查询调度信息
     * 
     * @param logininforId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    @Override
    public Logininfor selectLogininforById(Long logininforId)
    {
    	Optional<Logininfor> Op = logininforRepository.findById(logininforId);
    	if(Op.isPresent()) {
			return Op.get();
		}
        return null;
    }
    
	/**
     * 删除任务日志
     * @param logininforId 调度日志ID
     */
    @Override
    public void deleteLogininforById(Long logininforId)
    {
    	Logininfor logininfor = selectLogininforById(logininforId);
    	if(logininfor != null) {
    	//	logininfor.setRemoveStatus(RemoveEnum.REMOVE.getCode());
    	//	logininforRepository.save(logininfor);
    		logininforRepository.delete(logininfor);
    	}
    }
	
	/**
     * 批量删除系统登录日志
     * 
     * @param ids 需要删除的数据
     * @return
     */
    @Override
    public void deleteLogininforByIds(String ids)
    {
    	Long[] strArray = Convert.toLongArray(ids);
        for(Long id : strArray) {
        	deleteLogininforById(id);
        }
    }
    
    /**
     * 清空系统登录日志
     */
    @Override
    public void cleanLogininfor()
    {
//    	List<Logininfor> all = logininforRepository.findAll();
//    	for(Logininfor lf : all) {
//    		lf.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
//    		logininforRepository.save(lf);
//    	}
    	logininforRepository.deleteAll();
    }
}
