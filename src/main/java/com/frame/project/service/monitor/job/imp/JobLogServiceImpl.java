package com.frame.project.service.monitor.job.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.frame.common.enumutil.RemoveEnum;
import com.frame.common.utils.StringUtils;
import com.frame.common.utils.text.Convert;
import com.frame.project.domain.monitor.job.JobLog;
import com.frame.project.repository.monitor.job.JobLogRepository;
import com.frame.project.service.monitor.job.JobLogService;

/**
 * 
 * @className ：JobLogServiceImpl
 * @describe ：定时任务调度信息日志
 */
@Service
public class JobLogServiceImpl implements JobLogService {

	@Autowired
	private JobLogRepository jobLogRepository;
	
	/**
     * 分页
     * 获取quartz调度器日志的计划任务
     */
	@Override
	public Page<JobLog> findByPage(Integer pageNo, Integer pageSize, JobLog jobLog) {
		List<Sort.Order> orders = new ArrayList<>();
		orders.add(new Sort.Order(Sort.Direction.DESC, "id"));
		Sort sort = Sort.by(orders);
		Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
		
		Page<JobLog> page = jobLogRepository.findAll((root, query, cb) ->{
			List<Predicate> list = new ArrayList<>();
			list.add(cb.and(cb.equal(root.get("removeStatus").as(Integer.class), RemoveEnum.NOT_REMOVE.getCode())));
			
			if(StringUtils.isNotBlank(jobLog.getJobName())) {
				list.add(cb.and(cb.like(root.get("jobName").as(String.class), jobLog.getJobName())));
			}
			
			if(StringUtils.isNotBlank(jobLog.getJobGroup())) {
				list.add(cb.and(cb.equal(root.get("jobGroup").as(String.class), jobLog.getJobGroup())));
			}
			
			if(StringUtils.isNotBlank(jobLog.getStatus())) {
				list.add(cb.and(cb.equal(root.get("status").as(String.class), jobLog.getStatus())));
			}
			
			if(StringUtils.isNotBlank(jobLog.getInvokeTarget())) {
				list.add(cb.and(cb.like(root.get("invokeTarget").as(String.class), jobLog.getInvokeTarget())));
			}
			
			Predicate[] p = new Predicate[list.size()];
			return cb.and(list.toArray(p));
		},pageable);
		
		return page;
	}
	
	/**
	 * 新增
	 * @param jobLog
	 */
	@Override
	public void addJobLog(JobLog jobLog) {
		JobLog jl = new JobLog();
		jl.setJobName(jobLog.getJobName());
		jl.setJobGroup(jobLog.getJobGroup());
		jl.setInvokeTarget(jobLog.getInvokeTarget());
		jl.setJobMessage(jobLog.getJobMessage());
		jl.setStatus(jobLog.getStatus());
		jl.setExceptionInfo(jobLog.getExceptionInfo());
		jl.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		jobLogRepository.save(jl);
	}
	
	  /**
     * 通过调度任务日志ID查询调度信息
     * 
     * @param jobLogId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    @Override
    public JobLog selectJobLogById(Long jobLogId)
    {
    	Optional<JobLog> Op = jobLogRepository.findById(jobLogId);
    	if(Op.isPresent()) {
			return Op.get();
		}
        return null;
    }

    /**
     * 删除任务日志
     * （物理删除）
     * @param JobLogId 调度日志ID
     */
    @Override
    public void deleteJobLogById(Long JobLogId)
    {
    	JobLog jobLog = selectJobLogById(JobLogId);
    	if(jobLog != null) {
    		jobLog.setRemoveStatus(RemoveEnum.REMOVE.getCode());
    		jobLogRepository.save(jobLog);
    		//jobLogRepository.delete(jobLog);
    	}
    }
    
    /**
     * 批量删除调度日志信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public void deleteJobLogByIds(String ids)
    {
        Long[] strArray = Convert.toLongArray(ids);;
        for(Long id : strArray) {
        	deleteJobLogById(id);
        }
    }
    
    /**
     * 清空任务日志
     */
    @Override
    public void cleanJobLog()
    {
    	List<JobLog> all = jobLogRepository.findAll();
    	for(JobLog jl : all) {
    		jl.setRemoveStatus(RemoveEnum.REMOVE.getCode());
    		jobLogRepository.save(jl);
    	}
    	//jobLogRepository.deleteAll();
    }
}
