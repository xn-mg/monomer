package com.frame.project.service.monitor.job;

import org.quartz.SchedulerException;
import org.springframework.data.domain.Page;

import com.frame.common.exception.job.TaskException;
import com.frame.framework.web.service.BaseService;
import com.frame.project.domain.monitor.job.Job;

/**
 * 
 * @className ：JobService
 * @describe ：定时任务调度信息
 */
public interface JobService extends BaseService<Job>{

	/**
	 * 分页
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	Page<Job> findByPage(Integer pageNo, Integer pageSize, Job job);
	
	/**
     * 暂停任务
     * 
     * @param job 调度信息
     */
	void pauseJob(Job job) throws SchedulerException;
	
	/**
     * 恢复任务
     * 
     * @param job 调度信息
     */
	void resumeJob(Job job) throws SchedulerException;
	
	/**
     * 删除任务后，所对应的trigger也将被删除
     * 
     * @param job 调度信息
     */
	void deleteJob(Job job) throws SchedulerException;
	
	/**
     * 批量删除调度信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	void deleteJobByIds(String ids) throws SchedulerException;
	
	/**
     * 任务调度状态修改
     * 
     * @param job 调度信息
     */
	void changeStatus(Job job) throws SchedulerException;
	
	/**
     * 立即运行任务
     * 
     * @param job 调度信息
     */
	boolean run(Job job) throws SchedulerException;
	
	/**
     * 新增任务
     * 
     * @param job 调度信息 调度信息
     */
	void insertJob(Job job) throws SchedulerException, TaskException;
	
	/**
     * 更新任务的时间表达式
     * 
     * @param job 调度信息
     */
	void updateJob(Job job) throws SchedulerException, TaskException;
	
	/**
     * 校验cron表达式是否有效
     * 
     * @param cronExpression 表达式
     * @return 结果
     */
	boolean checkCronExpressionIsValid(String cronExpression);
}
