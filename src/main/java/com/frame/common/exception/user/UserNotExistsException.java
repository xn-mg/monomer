package com.frame.common.exception.user;

/**
 * 
 * @className ：UserNotExistsException
 * @describe ：用户不存在异常类
 */
public class UserNotExistsException extends UserException
{
    private static final long serialVersionUID = 1L;

    public UserNotExistsException()
    {
        super("user.not.exists", null);
    }
}
