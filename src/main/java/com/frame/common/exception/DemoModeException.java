package com.frame.common.exception;

/**
 * 
 * @className ：DemoModeException
 * @describe ：演示模式异常
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
