package com.frame.common.enumutil;

/**
 * 
 * @className ：UserStatusEnum
 * @describe ：用户账号状态
 */
public enum UserStatusEnum {

	OK(0, "正常"),
	
	DISABLE(1, "停用"),
	
	DELETED(2, "删除");
	
	private Integer code;

	private String message;

	private UserStatusEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
