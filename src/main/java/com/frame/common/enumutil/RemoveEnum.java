package com.frame.common.enumutil;

/**
 * 
 * @className ：RemoveEnum
 * @describe ：删除标记
 */
public enum RemoveEnum {

	/*
	 * 未删除
	 */
	NOT_REMOVE(0, "未删除"),

	/*
	 * 已删除
	 */
	REMOVE(1, "已删除");

	private Integer code;

	private String message;

	private RemoveEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
