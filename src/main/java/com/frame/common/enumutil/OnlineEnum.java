package com.frame.common.enumutil;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @className ：OnlineEnum
 * @describe ：用户状态
 */
public enum OnlineEnum {

	ON_LINE("在线",0,"on_line"), OFF_LINE("离线",1,"off_line");
	
	private String name;
	
	private Integer code;
	
	private String value;

	private OnlineEnum(String name, Integer code, String value) {
		this.name = name;
		this.code = code;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public static List<OnlineEnum> getList() {
		List<OnlineEnum> list = new ArrayList<>();
		list.add(OnlineEnum.ON_LINE);
		list.add(OnlineEnum.OFF_LINE);
		return list;
	}
}
