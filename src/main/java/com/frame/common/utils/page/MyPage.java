package com.frame.common.utils.page;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyPage<T> implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO() 
	*/ 
	private static final long serialVersionUID = 1L;
	
	private Integer number;
	
	private Integer totalPages;
	
	private List<T> content;
	
	private Pageable pageable;
	
	private String count;
	
	private Integer totalElements;

}
