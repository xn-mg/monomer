package com.frame.framework.aspectj.lang.enums;

/**
 * 
 * @className ：DataSourceType
 * @describe ：数据源
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
