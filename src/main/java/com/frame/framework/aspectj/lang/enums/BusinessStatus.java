package com.frame.framework.aspectj.lang.enums;

/**
 * 
 * @className ：BusinessStatus
 * @describe ：操作状态
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
