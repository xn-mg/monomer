package com.frame.framework.shiro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frame.common.constant.Constants;
import com.frame.common.constant.ShiroConstants;
import com.frame.common.constant.UserConstants;
import com.frame.common.enumutil.UserStatusEnum;
import com.frame.common.exception.user.CaptchaException;
import com.frame.common.exception.user.UserBlockedException;
import com.frame.common.exception.user.UserDeleteException;
import com.frame.common.exception.user.UserNotExistsException;
import com.frame.common.exception.user.UserPasswordNotMatchException;
import com.frame.common.utils.DateUtils;
import com.frame.common.utils.MessageUtils;
import com.frame.common.utils.ServletUtils;
import com.frame.common.utils.StringUtils;
import com.frame.common.utils.security.ShiroUtils;
import com.frame.framework.manager.AsyncManager;
import com.frame.framework.manager.factory.AsyncFactory;
import com.frame.project.domain.system.user.User;
import com.frame.project.service.system.user.UserService;

/**
 * 
 * @className ：LoginService
 * @describe ：登录校验方法
 */
@Component
public class LoginService
{
    @Autowired
    private PasswordService passwordService;

    @Autowired
    private UserService userService;

    /**
     * 登录
     */
    public User login(String username, String password)
    {
    	// 验证码校验
        if (ShiroConstants.CAPTCHA_ERROR.equals(ServletUtils.getRequest().getAttribute(ShiroConstants.CURRENT_CAPTCHA)))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
            throw new CaptchaException();
        }
        // 用户名或密码为空 错误
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("not.null")));
            throw new UserNotExistsException();
        }
        // 密码如果不在指定范围内 错误
        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH)
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new UserPasswordNotMatchException();
        }

        // 用户名不在指定范围内 错误
        if (username.length() < UserConstants.USERNAME_MIN_LENGTH
                || username.length() > UserConstants.USERNAME_MAX_LENGTH)
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new UserPasswordNotMatchException();
        }

        // 查询用户信息
        User user = userService.selectUserByLoginName(username);

//        if (user == null && maybeMobilePhoneNumber(username))
//        {
//            user = userService.selectUserByPhoneNumber(username);
//        }
//
//        if (user == null && maybeEmail(username))
//        {
//            user = userService.selectUserByEmail(username);
//        }

        if (user == null)
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.not.exists")));
            throw new UserNotExistsException();
        }
        
        if (UserStatusEnum.DELETED.getCode().equals(user.getRemoveStatus()))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.delete")));
            throw new UserDeleteException();
        }
        
        if (UserStatusEnum.DISABLE.getCode().equals(user.getStatus()))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.blocked", user.getRemark())));
            throw new UserBlockedException();
        }

        passwordService.validate(user, password);

        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        recordLoginInfo(user);
        return user;
    }

//    private boolean maybeEmail(String username)
//    {
//        if (!username.matches(UserConstants.EMAIL_PATTERN))
//        {
//            return false;
//        }
//        return true;
//    }
//
//    private boolean maybeMobilePhoneNumber(String username)
//    {
//        if (!username.matches(UserConstants.MOBILE_PHONE_NUMBER_PATTERN))
//        {
//            return false;
//        }
//        return true;
//    }

    /**
     * 记录登录信息
     */
    public void recordLoginInfo(User user)
    {
    	user.setLoginIp(ShiroUtils.getIp());
        user.setLoginDate(DateUtils.getNowDate());
        userService.updateUserInfo(user);
    }
}
