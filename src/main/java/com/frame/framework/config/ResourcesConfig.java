package com.frame.framework.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.frame.framework.interceptor.RepeatSubmitInterceptor;

/**
 * 
 * @className ：ResourcesConfig
 * @describe ：通用配置
 */
@Configuration
public class ResourcesConfig implements WebMvcConfigurer
{
	/**
     * 首页地址
     */
    @Value("${shiro.user.indexUrl}")
    private String indexUrl;

    @Autowired
    private RepeatSubmitInterceptor repeatSubmitInterceptor;
    
    /**
     * 默认首页的设置，当输入域名是可以自动跳转到默认指定的网页
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry)
    {
        registry.addViewController("/").setViewName("forward:" + indexUrl);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
    	registry.addResourceHandler("/**")
    	.addResourceLocations("classpath:/static/")
    	.addResourceLocations("classpath:/public/")
    	.addResourceLocations("/META-INF/resources/")
    	/** 本地文件上传路径 */
    	.addResourceLocations("file:" + MyFrameConfig.getProfile() + "/"); 

        /** swagger配置 */
        registry.addResourceHandler("/swagger-ui/**")
        .addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/");
		
    }

    /**
     * 自定义拦截规则
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(repeatSubmitInterceptor).addPathPatterns("/**");
    }
}