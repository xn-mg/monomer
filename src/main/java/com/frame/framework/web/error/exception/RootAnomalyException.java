package com.frame.framework.web.error.exception;

import com.frame.framework.web.error.enums.ExceptionEnum;
import com.frame.framework.web.error.enums.WebEnum;

/**
 * @className ：RootAnomalyException
 * @describe ：根异常
 */
public class RootAnomalyException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	private WebEnum webEnum;

	private ExceptionEnum flag;

	public RootAnomalyException(WebEnum webEnum) {
		super(webEnum.getMsg());
		this.webEnum = webEnum;
	}

	public RootAnomalyException(WebEnum webEnum,ExceptionEnum flag) {
		super(webEnum.getMsg());
		this.webEnum = webEnum;
		this.flag = flag;
	}

	public RootAnomalyException(String Msg) {
		super(Msg);
	}

	public WebEnum getWebEnum() {
		return webEnum;
	}

	public void setWebEnum(WebEnum webEnum) {
		this.webEnum = webEnum;
	}

	public ExceptionEnum getFlag() {
		return flag;
	}

	public void setFlag(ExceptionEnum flag) {
		this.flag = flag;
	}

}

