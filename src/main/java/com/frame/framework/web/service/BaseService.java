package com.frame.framework.web.service;

import java.util.List;

import com.frame.framework.web.error.exception.RootAnomalyException;



/**
 * @className ：BaseService
 * @describe ：业务层公共接口
 */
public interface BaseService<T> {
	/**
	 * 查询所有
	 * 
	 * @return List<T>
	 * @throws RootAnomalyException 
	 */
	public List<T> findAllList() throws RootAnomalyException;

	/** 
	 * add   新增数据
	 * 
	 * @param t 当前类型
	 * @return void
	 * @throws  RootAnomalyException
	 */
	public void add(T t) throws RootAnomalyException;

	/** 
	 * modify  修改数据
	 * 
	 * @param t 当前类型
	 * @return void
	 * @throws RootAnomalyException
	 */
	public void modify(T t) throws RootAnomalyException;

	/** 
	 * remove  逻辑删除 
	 * 
	 * @param   id 逻辑删除的对象的主键
	 * @return void
	 * @throws RootAnomalyException
	 */
	public void remove(Long id) throws RootAnomalyException;

	/** 
	 * delete  删除
	 * 
	 * @param  id 删除的对象的主键
	 * @return void
	 * @throws RootAnomalyException 有关联关系删除后抛出异常
	 */
	public void delete(Long id) throws RootAnomalyException;


	/** 
	 * findById  通过Id查询
	 * @param  id  主键
	 * @return T 返回当前类型
	 * @throws RootAnomalyException
	 */
	public T findById(Long id) throws RootAnomalyException;
	
	
	/** 
	 * 根据 removeStatus 查看集合
	 * @see findAllByRs 
	 * @thorows 
	 * @return List<T>
	 */
	public List<T> findAllByRs(Integer status);

}
