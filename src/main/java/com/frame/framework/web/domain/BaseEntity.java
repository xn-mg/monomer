package com.frame.framework.web.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import lombok.Data;

/**
 * @className ：PersistentObject
 * @describe ：基类
 */
@MappedSuperclass
@Data
public abstract class BaseEntity implements Serializable {
	/** 
	* @Fields serialVersionUID : TODO() 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/** 
	    * 创建者
	  */
	@Column(name = "create_by")
    private String createBy;
	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;
	/** 
	  * 更新者 
	 */
	@Column(name = "update_by")
    private String updateBy;
	/**
	 * 修改时间
	 */
	@Column(name = "update_time")
	private Date updateTime;
	/**
	 *  显示顺序
	 */
	@Column(name = "order_num")
	private Integer orderNum;
	/** 
	  * 备注
	 */
	@Column(columnDefinition = "longtext", nullable = true)
    private String remark;
	/**
	 * 状态 0：激活 1：冻结
	 */
	@Column(name = "remove_status")
	private Integer removeStatus;
	
	/** 请求参数 */
	@Transient //不在数据库中做映射
    private Map<String, Object> params;
	
	/** 搜索值 */
	@Transient //不在数据库中做映射
    private String searchValue;

}
