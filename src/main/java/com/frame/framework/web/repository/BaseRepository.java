package com.frame.framework.web.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;


/**
 * @className ：BaseRepository
 * @describe ：持久层接口
 */
@NoRepositoryBean //告诉JPA不要创建对应接口的bean对象
public interface BaseRepository<T  extends Serializable,ID> extends JpaRepository<T, ID>,JpaSpecificationExecutor<T>{
	
}
