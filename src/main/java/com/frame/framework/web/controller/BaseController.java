package com.frame.framework.web.controller;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.frame.common.utils.DateUtils;
import com.frame.common.utils.StringUtils;
import com.frame.common.utils.page.MyPage;
import com.frame.common.utils.page.PageUtils;
import com.frame.common.utils.security.ShiroUtils;
import com.frame.common.utils.sql.SqlUtil;
import com.frame.framework.web.error.WebError;
import com.frame.framework.web.error.enums.WebEnum;
import com.frame.framework.web.page.PageDomain;
import com.frame.framework.web.page.TableDataInfo;
import com.frame.framework.web.page.TableSupport;
import com.frame.project.domain.system.user.User;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 * 
 * @className ：BaseController
 * @describe ：web层通用数据处理
 */
public class BaseController
{
    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder)
    {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage()
    {
    	PageUtils.startPage();
		
    }

    /**
     * 设置请求排序数据
     */
    protected void startOrderBy()
    {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        if (StringUtils.isNotEmpty(pageDomain.getOrderBy()))
        {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            PageHelper.orderBy(orderBy);
        }
    }
    
    /**
     * 清理分页的线程变量
     */
    protected void clearPage()
    {
        PageUtils.clearPage();
    }

    /**
     * 响应请求分页数据
     */
    protected TableDataInfo getDataTable(Page<?> list)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(list.getContent());
        rspData.setTotal(list.getTotalElements());
        return rspData;
    }
    
    /**
     * 响应请求分页数据
     */
    protected TableDataInfo getDataTable(MyPage<?> list)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(list.getContent());
        rspData.setTotal(list.getTotalElements());
        return rspData;
    }
    
    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected TableDataInfo getDataTable(List<?> list)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 返回成功
     */
    public WebError success()
    {
        return new WebError();
    }

    /**
     * 返回错误码消息
     */
    public WebError success(Integer code, String message)
    {
        return new WebError(code, message);
    }

    /**
     * 返回失败消息
     */
    public WebError error(WebEnum webEnum)
    {
        return new WebError(webEnum);
    }

    /**
     * 返回错误码消息
     */
    public WebError error(Integer code, String message)
    {
        return new WebError(code, message);
    }

    /**
     * 页面跳转
     */
    public String redirect(String url)
    {
        return StringUtils.format("redirect:{}", url);
    }

    public User getSysUser()
    {
        return ShiroUtils.getSysUser();
    }

    public void setSysUser(User user)
    {
        ShiroUtils.setSysUser(user);
    }

    public Long getUserId()
    {
        return getSysUser().getId();
    }

    public String getLoginName()
    {
        return getSysUser().getLoginName();
    }
}
