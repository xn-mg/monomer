package com.frame.framework.utils.quartz;

import org.quartz.JobExecutionContext;

import com.frame.project.domain.monitor.job.Job;

/**
 * 
 * @className ：QuartzJobExecution
 * @describe ：定时任务处理（允许并发执行）
 */
public class QuartzJobExecution extends AbstractQuartzJob
{
    @Override
    protected void doExecute(JobExecutionContext context, Job job) throws Exception
    {
        JobInvokeUtil.invokeMethod(job);
    }
}
