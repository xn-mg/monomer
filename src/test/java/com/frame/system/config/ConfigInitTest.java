package com.frame.system.config;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.frame.common.enumutil.RemoveEnum;
import com.frame.project.domain.system.config.Config;
import com.frame.project.repository.system.config.ConfigRepository;

/**
 * 
 * @className ：ConfigInitTest
 * @describe ：参数配置测试类
 * @author ：Xn 
 * @date ：2020年6月4日 
 * Copyright(C) 2020,长春市海纳软件信息技术有限公司
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ConfigInitTest {

	@Autowired
	private ConfigRepository configRepository;
	
	@Test
	public void initConfig() {
		Config config = new Config();
		config.setConfigName("主框架页-默认皮肤样式名称");
		config.setConfigKey("sys.index.skinName");
		config.setConfigValue("skin-blue");
		config.setConfigType("Y");
		config.setCreateBy("admin");
		config.setCreateTime(new Date());
		config.setUpdateTime(new Date());
		config.setRemark("蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow");
		config.setOrderNum(0);
		config.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		
		Config config1 = new Config();
		config1.setConfigName("用户管理-账号初始密码");
		config1.setConfigKey("sys.user.initPassword");
		config1.setConfigValue("123456");
		config1.setConfigType("Y");
		config1.setCreateBy("admin");
		config1.setCreateTime(new Date());
		config1.setUpdateTime(new Date());
		config1.setRemark("初始化密码 123456");
		config1.setOrderNum(1);
		config1.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		
		Config config2 = new Config();
		config2.setConfigName("主框架页-侧边栏主题");
		config2.setConfigKey("sys.index.sideTheme");
		config2.setConfigValue("theme-dark");
		config2.setConfigType("Y");
		config2.setCreateBy("admin");
		config2.setCreateTime(new Date());
		config2.setUpdateTime(new Date());
		config2.setRemark("深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue");
		config2.setOrderNum(2);
		config2.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		
		Config config3 = new Config();
		config3.setConfigName("账号自助-是否开启用户注册功能");
		config3.setConfigKey("sys.account.registerUser");
		config3.setConfigValue("true");
		config3.setConfigType("Y");
		config3.setCreateBy("admin");
		config3.setCreateTime(new Date());
		config3.setUpdateTime(new Date());
		config3.setRemark("是否开启注册用户功能");
		config3.setOrderNum(3);
		config3.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		
		configRepository.save(config);
		configRepository.save(config1);
		configRepository.save(config2);
		configRepository.save(config3);
	}
}
