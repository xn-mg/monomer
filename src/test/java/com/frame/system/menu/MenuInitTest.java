package com.frame.system.menu;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.frame.common.enumutil.RemoveEnum;
import com.frame.project.domain.system.menu.Menu;
import com.frame.project.form.system.menu.MenuNormal;
import com.frame.project.repository.system.menu.MenuRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MenuInitTest {

	@Autowired
	private MenuRepository menuRepository;
	
	@Test
	public void initMenu() {
		//一级菜单
		Menu menu1 = new Menu();
		menu1.setMenuName("系统管理");
		menu1.setParentId(0L);
		menu1.setOrderNum(1);
		menu1.setUrl("#");
		menu1.setMenuType("M");
		menu1.setVisible("0");
		menu1.setIcon("fa fa-gear");
		menu1.setCreateBy("admin");
		menu1.setCreateTime(new Date());
		menu1.setRemark("系统管理目录");
		menu1.setTarget("");
		menu1.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		menuRepository.save(menu1);
		
		
		Menu menu2 = new Menu();
		menu2.setMenuName("系统监控");
		menu2.setParentId(0L);
		menu2.setOrderNum(2);
		menu2.setUrl("#");
		menu2.setMenuType("M");
		menu2.setVisible("0");
		menu2.setIcon("fa fa-video-camera");
		menu2.setCreateBy("admin");
		menu2.setCreateTime(new Date());
		menu2.setRemark("系统监控目录");
		menu2.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		menuRepository.save(menu2);
		
		//二级菜单
		Menu menu1_1 = new Menu();
		menu1_1.setMenuName("菜单管理");
		menu1_1.setParentId(menu1.getId());
		menu1_1.setOrderNum(1);
		menu1_1.setUrl("/system/menu");
		menu1_1.setPerms("system:menu:view");
		menu1_1.setMenuType("C");
		menu1_1.setVisible("0");
		menu1_1.setIcon("#");
		menu1_1.setCreateBy("admin");
		menu1_1.setCreateTime(new Date());
		menu1_1.setRemark("菜单管理菜单");
		menu1_1.setTarget("");
		menu1_1.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		menuRepository.save(menu1_1);
		
	}
	
	@Test
	public void test1() {
		List<Map<String, Object>> a = menuRepository.selectMenuNormalAll();
		for(int i = 0; i < a.size(); i++) {
			Map<String, Object> map = a.get(i);
			for(String key : map.keySet()){
			    System.out.println(key);
			}
			/*
			 * for(Object value : map.values()){ System.out.println(value); }
			 */
		}
	}
}
