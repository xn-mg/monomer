package com.frame.system.user;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.frame.common.enumutil.RemoveEnum;
import com.frame.framework.shiro.service.PasswordService;
import com.frame.project.domain.system.user.User;
import com.frame.project.repository.system.user.UserRepository;

/**
 * 
 * @className ：UserInitTest
 * @describe ：用户测试类
 * @author ：Xn 
 * @date ：2020年6月4日 
 * Copyright(C) 2020,长春市海纳软件信息技术有限公司
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserInitTest {

	@Autowired
    private PasswordService passwordService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void initUser() {
		User user = new User();
		user.setLoginName("admin");
		user.setUserName("超管");
		user.setEmail("123@qq.com");
		user.setPhonenumber("18888888888");
		user.setSex("1");
		user.setAvatar("");
		user.randomSalt();
		user.setPassword(passwordService.encryptPassword("admin", "123456", user.getSalt()));
		user.setStatus("0");
		user.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		user.setLoginIp("127.0.0.1");
		user.setLoginDate(new Date());
		user.setCreateBy("admin");
		user.setCreateTime(new Date());
		user.setRemark("超级管理员");
		userRepository.save(user);
	}
}
