package com.frame.system.dept;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.frame.common.enumutil.RemoveEnum;
import com.frame.project.domain.system.dept.Dept;
import com.frame.project.repository.system.dept.DeptRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DeptInitTest {

	@Autowired
	private DeptRepository deptRepository;
	
	@Test
	public void initDept() {
		Dept dept = new Dept();
		dept.setDeptName("海纳软件");
		dept.setParentId(0L);
		dept.setAncestors("0");
		dept.setOrderNum(0);
		dept.setLeader("曹");
		dept.setPhone("18888888888");
		dept.setEmail("123@qq.com");
		dept.setStatus("0");
		dept.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dept.setCreateBy("admin");
		dept.setCreateTime(new Date());
		deptRepository.save(dept);
		
	}
}
