package com.frame.system.dict;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.frame.common.enumutil.RemoveEnum;
import com.frame.project.domain.system.dict.DictData;
import com.frame.project.repository.system.dict.DictDataRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataInitTest {

	@Autowired
	private DictDataRepository dictDataRepository;
	
	@Test
	public void initData() {
		DictData dd1 = new DictData();
		dd1.setDictSort(1);
		dd1.setDictLabel("男");
		dd1.setDictValue("0");
		dd1.setDictType("sys_user_sex");
		dd1.setListClass("");
		dd1.setIsDefault("Y");
		dd1.setStatus("0");
		dd1.setCreateBy("admin");
		dd1.setCreateTime(new Date());
		dd1.setRemark("性别男");
		dd1.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd1);
		
		DictData dd2 = new DictData();
		dd2.setDictSort(2);
		dd2.setDictLabel("女");
		dd2.setDictValue("1");
		dd2.setDictType("sys_user_sex");
		dd2.setListClass("");
		dd2.setIsDefault("N");
		dd2.setStatus("0");
		dd2.setCreateBy("admin");
		dd2.setCreateTime(new Date());
		dd2.setRemark("性别女");
		dd2.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd2);
		
		DictData dd3 = new DictData();
		dd3.setDictSort(3);
		dd3.setDictLabel("未知");
		dd3.setDictValue("2");
		dd3.setDictType("sys_user_sex");
		dd3.setListClass("");
		dd3.setIsDefault("N");
		dd3.setStatus("0");
		dd3.setCreateBy("admin");
		dd3.setCreateTime(new Date());
		dd3.setRemark("性别未知");
		dd3.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd3);
		
		
		
		DictData dd4 = new DictData();
		dd4.setDictSort(1);
		dd4.setDictLabel("显示");
		dd4.setDictValue("0");
		dd4.setDictType("sys_show_hide");
		dd4.setListClass("primary");
		dd4.setIsDefault("Y");
		dd4.setStatus("0");
		dd4.setCreateBy("admin");
		dd4.setCreateTime(new Date());
		dd4.setRemark("显示菜单");
		dd4.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd4);
		
		DictData dd5 = new DictData();
		dd5.setDictSort(2);
		dd5.setDictLabel("隐藏");
		dd5.setDictValue("1");
		dd5.setDictType("sys_show_hide");
		dd5.setListClass("danger");
		dd5.setIsDefault("N");
		dd5.setStatus("0");
		dd5.setCreateBy("admin");
		dd5.setCreateTime(new Date());
		dd5.setRemark("隐藏菜单");
		dd5.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd5);
		
		
		
		DictData dd6 = new DictData();
		dd6.setDictSort(1);
		dd6.setDictLabel("正常");
		dd6.setDictValue("0");
		dd6.setDictType("sys_normal_disable");
		dd6.setListClass("primary");
		dd6.setIsDefault("Y");
		dd6.setStatus("0");
		dd6.setCreateBy("admin");
		dd6.setCreateTime(new Date());
		dd6.setRemark("正常状态");
		dd6.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd6);
		
		DictData dd7 = new DictData();
		dd7.setDictSort(2);
		dd7.setDictLabel("停用");
		dd7.setDictValue("1");
		dd7.setDictType("sys_normal_disable");
		dd7.setListClass("danger");
		dd7.setIsDefault("N");
		dd7.setStatus("0");
		dd7.setCreateBy("admin");
		dd7.setCreateTime(new Date());
		dd7.setRemark("停用状态");
		dd7.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd7);
		
		
		
		DictData dd8 = new DictData();
		dd8.setDictSort(1);
		dd8.setDictLabel("正常");
		dd8.setDictValue("0");
		dd8.setDictType("sys_job_status");
		dd8.setListClass("primary");
		dd8.setIsDefault("Y");
		dd8.setStatus("0");
		dd8.setCreateBy("admin");
		dd8.setCreateTime(new Date());
		dd8.setRemark("正常状态");
		dd8.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd8);
		
		DictData dd9 = new DictData();
		dd9.setDictSort(2);
		dd9.setDictLabel("暂停");
		dd9.setDictValue("1");
		dd9.setDictType("sys_job_status");
		dd9.setListClass("danger");
		dd9.setIsDefault("N");
		dd9.setStatus("0");
		dd9.setCreateBy("admin");
		dd9.setCreateTime(new Date());
		dd9.setRemark("停用状态");
		dd9.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd9);
		
		
		
		DictData dd10 = new DictData();
		dd10.setDictSort(1);
		dd10.setDictLabel("默认");
		dd10.setDictValue("DEFAULT");
		dd10.setDictType("sys_job_group");
		dd10.setListClass("");
		dd10.setIsDefault("Y");
		dd10.setStatus("0");
		dd10.setCreateBy("admin");
		dd10.setCreateTime(new Date());
		dd10.setRemark("默认分组");
		dd10.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd10);
		
		DictData dd11 = new DictData();
		dd11.setDictSort(2);
		dd11.setDictLabel("系统");
		dd11.setDictValue("SYSTEM");
		dd11.setDictType("sys_job_group");
		dd11.setListClass("");
		dd11.setIsDefault("N");
		dd11.setStatus("0");
		dd11.setCreateBy("admin");
		dd11.setCreateTime(new Date());
		dd11.setRemark("系统分组");
		dd11.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd11);
		
		
		
		DictData dd12 = new DictData();
		dd12.setDictSort(1);
		dd12.setDictLabel("是");
		dd12.setDictValue("Y");
		dd12.setDictType("sys_yes_no");
		dd12.setListClass("primary");
		dd12.setIsDefault("Y");
		dd12.setStatus("0");
		dd12.setCreateBy("admin");
		dd12.setCreateTime(new Date());
		dd12.setRemark("系统默认是");
		dd12.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd12);
		
		DictData dd13 = new DictData();
		dd13.setDictSort(2);
		dd13.setDictLabel("否");
		dd13.setDictValue("N");
		dd13.setDictType("sys_yes_no");
		dd13.setListClass("danger");
		dd13.setIsDefault("N");
		dd13.setStatus("0");
		dd13.setCreateBy("admin");
		dd13.setCreateTime(new Date());
		dd13.setRemark("系统默认否");
		dd13.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd13);
		
		
		
		DictData dd14 = new DictData();
		dd14.setDictSort(1);
		dd14.setDictLabel("通知");
		dd14.setDictValue("1");
		dd14.setDictType("sys_notice_type");
		dd14.setListClass("warning");
		dd14.setIsDefault("Y");
		dd14.setStatus("0");
		dd14.setCreateBy("admin");
		dd14.setCreateTime(new Date());
		dd14.setRemark("通知");
		dd14.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd14);
		
		DictData dd15 = new DictData();
		dd15.setDictSort(2);
		dd15.setDictLabel("公告");
		dd15.setDictValue("2");
		dd15.setDictType("sys_notice_type");
		dd15.setListClass("success");
		dd15.setIsDefault("N");
		dd15.setStatus("0");
		dd15.setCreateBy("admin");
		dd15.setCreateTime(new Date());
		dd15.setRemark("公告");
		dd15.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd15);
		
		
		
		DictData dd16 = new DictData();
		dd16.setDictSort(1);
		dd16.setDictLabel("正常");
		dd16.setDictValue("0");
		dd16.setDictType("sys_notice_status");
		dd16.setListClass("primary");
		dd16.setIsDefault("Y");
		dd16.setStatus("0");
		dd16.setCreateBy("admin");
		dd16.setCreateTime(new Date());
		dd16.setRemark("正常状态");
		dd16.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd16);
		
		DictData dd17 = new DictData();
		dd17.setDictSort(2);
		dd17.setDictLabel("关闭");
		dd17.setDictValue("1");
		dd17.setDictType("sys_notice_status");
		dd17.setListClass("danger");
		dd17.setIsDefault("N");
		dd17.setStatus("0");
		dd17.setCreateBy("admin");
		dd17.setCreateTime(new Date());
		dd17.setRemark("关闭状态");
		dd17.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd17);
		
		
		
		DictData dd18 = new DictData();
		dd18.setDictSort(99);
		dd18.setDictLabel("其他");
		dd18.setDictValue("0");
		dd18.setDictType("sys_oper_type");
		dd18.setListClass("info");
		dd18.setIsDefault("N");
		dd18.setStatus("0");
		dd18.setCreateBy("admin");
		dd18.setCreateTime(new Date());
		dd18.setRemark("其他操作");
		dd18.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd18);
		
		DictData dd19 = new DictData();
		dd19.setDictSort(1);
		dd19.setDictLabel("新增");
		dd19.setDictValue("1");
		dd19.setDictType("sys_oper_type");
		dd19.setListClass("info");
		dd19.setIsDefault("N");
		dd19.setStatus("0");
		dd19.setCreateBy("admin");
		dd19.setCreateTime(new Date());
		dd19.setRemark("新增操作");
		dd19.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd19);
		
		DictData dd20 = new DictData();
		dd20.setDictSort(2);
		dd20.setDictLabel("修改");
		dd20.setDictValue("2");
		dd20.setDictType("sys_oper_type");
		dd20.setListClass("info");
		dd20.setIsDefault("N");
		dd20.setStatus("0");
		dd20.setCreateBy("admin");
		dd20.setCreateTime(new Date());
		dd20.setRemark("修改操作");
		dd20.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd20);
		
		DictData dd21 = new DictData();
		dd21.setDictSort(3);
		dd21.setDictLabel("删除");
		dd21.setDictValue("3");
		dd21.setDictType("sys_oper_type");
		dd21.setListClass("danger");
		dd21.setIsDefault("N");
		dd21.setStatus("0");
		dd21.setCreateBy("admin");
		dd21.setCreateTime(new Date());
		dd21.setRemark("删除操作");
		dd21.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd21);
		
		DictData dd22 = new DictData();
		dd22.setDictSort(4);
		dd22.setDictLabel("授权");
		dd22.setDictValue("4");
		dd22.setDictType("sys_oper_type");
		dd22.setListClass("primary");
		dd22.setIsDefault("N");
		dd22.setStatus("0");
		dd22.setCreateBy("admin");
		dd22.setCreateTime(new Date());
		dd22.setRemark("授权操作");
		dd22.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd22);
		
		DictData dd23 = new DictData();
		dd23.setDictSort(5);
		dd23.setDictLabel("导出");
		dd23.setDictValue("5");
		dd23.setDictType("sys_oper_type");
		dd23.setListClass("warning");
		dd23.setIsDefault("N");
		dd23.setStatus("0");
		dd23.setCreateBy("admin");
		dd23.setCreateTime(new Date());
		dd23.setRemark("导出操作");
		dd23.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd23);
		
		DictData dd24 = new DictData();
		dd24.setDictSort(6);
		dd24.setDictLabel("导入");
		dd24.setDictValue("6");
		dd24.setDictType("sys_oper_type");
		dd24.setListClass("warning");
		dd24.setIsDefault("N");
		dd24.setStatus("0");
		dd24.setCreateBy("admin");
		dd24.setCreateTime(new Date());
		dd24.setRemark("导入操作");
		dd24.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd24);
		
		DictData dd25 = new DictData();
		dd25.setDictSort(7);
		dd25.setDictLabel("强退");
		dd25.setDictValue("7");
		dd25.setDictType("sys_oper_type");
		dd25.setListClass("danger");
		dd25.setIsDefault("N");
		dd25.setStatus("0");
		dd25.setCreateBy("admin");
		dd25.setCreateTime(new Date());
		dd25.setRemark("强退操作");
		dd25.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd25);
		
		DictData dd26 = new DictData();
		dd26.setDictSort(8);
		dd26.setDictLabel("清空数据");
		dd26.setDictValue("8");
		dd26.setDictType("sys_oper_type");
		dd26.setListClass("danger");
		dd26.setIsDefault("N");
		dd26.setStatus("0");
		dd26.setCreateBy("admin");
		dd26.setCreateTime(new Date());
		dd26.setRemark("清空数据");
		dd26.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd26);
		
		
		
		DictData dd27 = new DictData();
		dd27.setDictSort(1);
		dd27.setDictLabel("成功");
		dd27.setDictValue("0");
		dd27.setDictType("sys_common_status");
		dd27.setListClass("primary");
		dd27.setIsDefault("N");
		dd27.setStatus("0");
		dd27.setCreateBy("admin");
		dd27.setCreateTime(new Date());
		dd27.setRemark("正常状态");
		dd27.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd27);
		
		DictData dd28 = new DictData();
		dd28.setDictSort(2);
		dd28.setDictLabel("失败");
		dd28.setDictValue("1");
		dd28.setDictType("sys_common_status");
		dd28.setListClass("danger");
		dd28.setIsDefault("N");
		dd28.setStatus("0");
		dd28.setCreateBy("admin");
		dd28.setCreateTime(new Date());
		dd28.setRemark("停用状态");
		dd28.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictDataRepository.save(dd28);
	}
}
