package com.frame.system.dict;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.frame.common.enumutil.RemoveEnum;
import com.frame.project.domain.system.dict.DictType;
import com.frame.project.repository.system.dict.DictTypeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TypeInitTest {

	@Autowired
	private DictTypeRepository dictTypeRepository;
	
	@Test
	public void initType() {
		DictType dt1 = new DictType();
		dt1.setDictName("用户性别");
		dt1.setDictTypes("sys_user_sex");
		dt1.setStatus("0");
		dt1.setCreateBy("admin");
		dt1.setCreateTime(new Date());
		dt1.setRemark("用户性别列表");
		dt1.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictTypeRepository.save(dt1);
		
		DictType dt2 = new DictType();
		dt2.setDictName("菜单状态");
		dt2.setDictTypes("sys_show_hide");
		dt2.setStatus("0");
		dt2.setCreateBy("admin");
		dt2.setCreateTime(new Date());
		dt2.setRemark("菜单状态列表");
		dt2.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictTypeRepository.save(dt2);
		
		DictType dt3 = new DictType();
		dt3.setDictName("系统开关");
		dt3.setDictTypes("sys_normal_disable");
		dt3.setStatus("0");
		dt3.setCreateBy("admin");
		dt3.setCreateTime(new Date());
		dt3.setRemark("系统开关列表");
		dt3.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictTypeRepository.save(dt3);
		
		DictType dt4 = new DictType();
		dt4.setDictName("任务状态");
		dt4.setDictTypes("sys_job_status");
		dt4.setStatus("0");
		dt4.setCreateBy("admin");
		dt4.setCreateTime(new Date());
		dt4.setRemark("任务状态列表");
		dt4.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictTypeRepository.save(dt4);
		
		DictType dt5 = new DictType();
		dt5.setDictName("任务分组");
		dt5.setDictTypes("sys_job_group");
		dt5.setStatus("0");
		dt5.setCreateBy("admin");
		dt5.setCreateTime(new Date());
		dt5.setRemark("任务分组列表");
		dt5.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictTypeRepository.save(dt5);
		
		DictType dt6 = new DictType();
		dt6.setDictName("系统是否");
		dt6.setDictTypes("sys_yes_no");
		dt6.setStatus("0");
		dt6.setCreateBy("admin");
		dt6.setCreateTime(new Date());
		dt6.setRemark("系统是否列表");
		dt6.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictTypeRepository.save(dt6);
		
		DictType dt7 = new DictType();
		dt7.setDictName("通知类型");
		dt7.setDictTypes("sys_notice_type");
		dt7.setStatus("0");
		dt7.setCreateBy("admin");
		dt7.setCreateTime(new Date());
		dt7.setRemark("通知类型列表");
		dt7.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictTypeRepository.save(dt7);
		
		DictType dt8 = new DictType();
		dt8.setDictName("通知状态");
		dt8.setDictTypes("sys_notice_status");
		dt8.setStatus("0");
		dt8.setCreateBy("admin");
		dt8.setCreateTime(new Date());
		dt8.setRemark("通知状态列表");
		dt8.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictTypeRepository.save(dt8);
		
		DictType dt9 = new DictType();
		dt9.setDictName("操作类型");
		dt9.setDictTypes("sys_oper_type");
		dt9.setStatus("0");
		dt9.setCreateBy("admin");
		dt9.setCreateTime(new Date());
		dt9.setRemark("操作类型列表");
		dt9.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictTypeRepository.save(dt9);
		
		DictType dt10 = new DictType();
		dt10.setDictName("系统状态");
		dt10.setDictTypes("sys_common_status");
		dt10.setStatus("0");
		dt10.setCreateBy("admin");
		dt10.setCreateTime(new Date());
		dt10.setRemark("系统状态列表");
		dt10.setRemoveStatus(RemoveEnum.NOT_REMOVE.getCode());
		dictTypeRepository.save(dt10);
	}
}
