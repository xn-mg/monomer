/*
 Navicat Premium Data Transfer

 Source Server         : localhost_5.5
 Source Server Type    : MySQL
 Source Server Version : 50519
 Source Host           : localhost:3307
 Source Schema         : base-frame

 Target Server Type    : MySQL
 Target Server Version : 50519
 File Encoding         : 65001

 Date: 11/06/2022 13:23:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `blob_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('MyFrameScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '* * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('MyFrameScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '* * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('MyFrameScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '* * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fired_time` bigint(20) NOT NULL,
  `sched_time` bigint(20) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('MyFrameScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.frame.framework.utils.quartz.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E6672616D652E70726F6A6563742E646F6D61696E2E6D6F6E69746F722E6A6F622E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000D6E65787456616C696454696D657400104C6A6176612F7574696C2F446174653B4C000673746174757371007E000978720029636F6D2E6672616D652E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E74697479000000000000000102000A4C0008637265617465427971007E00094C000A63726561746554696D6571007E000A4C000269647400104C6A6176612F6C616E672F4C6F6E673B4C00086F726465724E756D7400134C6A6176612F6C616E672F496E74656765723B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000C72656D6F766553746174757371007E000D4C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000A787074000561646D696E737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017332DDAF8078000000007372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B020000787000000000000000017070740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E00140000000070707371007E001077080000017332DDAF8078000000007400013174000B2A202A202A202A202A203F7400116D665461736B2E6D664E6F506172616D7374000744454641554C54740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC897400013370740001317800);
INSERT INTO `qrtz_job_details` VALUES ('MyFrameScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.frame.framework.utils.quartz.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E6672616D652E70726F6A6563742E646F6D61696E2E6D6F6E69746F722E6A6F622E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000D6E65787456616C696454696D657400104C6A6176612F7574696C2F446174653B4C000673746174757371007E000978720029636F6D2E6672616D652E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E74697479000000000000000102000A4C0008637265617465427971007E00094C000A63726561746554696D6571007E000A4C000269647400104C6A6176612F6C616E672F4C6F6E673B4C00086F726465724E756D7400134C6A6176612F6C616E672F496E74656765723B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000C72656D6F766553746174757371007E000D4C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000A787074000561646D696E737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017332DE6EE878000000007372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B020000787000000000000000027070740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E00140000000070707371007E001077080000017332DE6EE878000000007400013174000B2A202A202A202A202A203F7400156D665461736B2E6D66506172616D7328276D66272974000744454641554C54740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC897400013370740001317800);
INSERT INTO `qrtz_job_details` VALUES ('MyFrameScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.frame.framework.utils.quartz.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E6672616D652E70726F6A6563742E646F6D61696E2E6D6F6E69746F722E6A6F622E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000D6E65787456616C696454696D657400104C6A6176612F7574696C2F446174653B4C000673746174757371007E000978720029636F6D2E6672616D652E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E74697479000000000000000102000A4C0008637265617465427971007E00094C000A63726561746554696D6571007E000A4C000269647400104C6A6176612F6C616E672F4C6F6E673B4C00086F726465724E756D7400134C6A6176612F6C616E672F496E74656765723B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000C72656D6F766553746174757371007E000D4C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000A787074000561646D696E737200126A6176612E73716C2E54696D657374616D702618D5C80153BF650200014900056E616E6F737872000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017332DF8C1078000000007372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B020000787000000000000000037070740000737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E00140000000070707371007E001077080000017332DF8C1078000000007400013174000B2A202A202A202A202A203F7400386D665461736B2E6D664D756C7469706C65506172616D7328276D66272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C54740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC897400013370740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('MyFrameScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('MyFrameScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_checkin_time` bigint(20) NOT NULL,
  `checkin_interval` bigint(20) NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('MyFrameScheduler', 'DESKTOP-2Q5E3L11654911467410', 1654924882367, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `repeat_count` bigint(20) NOT NULL,
  `repeat_interval` bigint(20) NOT NULL,
  `times_triggered` bigint(20) NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `int_prop_1` int(11) NULL DEFAULT NULL,
  `int_prop_2` int(11) NULL DEFAULT NULL,
  `long_prop_1` bigint(20) NULL DEFAULT NULL,
  `long_prop_2` bigint(20) NULL DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `next_fire_time` bigint(20) NULL DEFAULT NULL,
  `prev_fire_time` bigint(20) NULL DEFAULT NULL,
  `priority` int(11) NULL DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `start_time` bigint(20) NOT NULL,
  `end_time` bigint(20) NULL DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `misfire_instr` smallint(6) NULL DEFAULT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('MyFrameScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1654911467000, -1, 5, 'PAUSED', 'CRON', 1654911467000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('MyFrameScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1654911467000, -1, 5, 'PAUSED', 'CRON', 1654911467000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('MyFrameScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1654911467000, -1, 5, 'PAUSED', 'CRON', 1654911467000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `config_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'admin', '2020-07-09 16:24:15', 0, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow', 0, NULL, '2020-07-09 16:24:15', 'sys.index.skinName', '主框架页-默认皮肤样式名称', 'Y', 'skin-blue');
INSERT INTO `sys_config` VALUES (2, 'admin', '2020-07-09 16:24:15', 1, '初始化密码 123456', 0, NULL, '2020-07-09 16:24:15', 'sys.user.initPassword', '用户管理-账号初始密码', 'Y', '123456');
INSERT INTO `sys_config` VALUES (3, 'admin', '2020-07-09 16:24:15', 2, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue', 0, NULL, '2020-07-09 16:24:15', 'sys.index.sideTheme', '主框架页-侧边栏主题', 'Y', 'theme-dark');
INSERT INTO `sys_config` VALUES (4, 'admin', '2020-07-09 16:24:15', 3, '是否开启注册用户功能', 0, NULL, '2020-07-09 16:24:15', 'sys.account.registerUser', '账号自助-是否开启用户注册功能', 'Y', 'true');
INSERT INTO `sys_config` VALUES (5, 'admin', '2022-06-10 16:05:04', 4, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&amp;*()-=_+）', 0, NULL, NULL, 'sys.account.chrtype', '用户管理-密码字符范围', 'Y', '0');
INSERT INTO `sys_config` VALUES (6, 'admin', '2022-06-10 16:05:49', 5, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框', 0, NULL, NULL, 'sys.account.initPasswordModify', '用户管理-初始密码修改策略', 'Y', '0');
INSERT INTO `sys_config` VALUES (7, 'admin', '2022-06-10 16:06:22', 6, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框', 0, NULL, NULL, 'sys.account.passwordValidateDays', '用户管理-账号密码更新周期', 'Y', '0');
INSERT INTO `sys_config` VALUES (8, 'admin', '2022-06-10 16:06:51', 7, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）', 0, NULL, NULL, 'sys.index.menuStyle', '主框架页-菜单导航显示风格', 'Y', 'topnav');
INSERT INTO `sys_config` VALUES (9, 'admin', '2022-06-10 16:07:17', 8, '是否开启底部页脚显示（true显示，false隐藏）', 0, NULL, NULL, 'sys.index.footer', '主框架页-是否开启页脚', 'Y', 'true');
INSERT INTO `sys_config` VALUES (10, 'admin', '2022-06-10 16:07:37', 9, '是否开启菜单多页签显示（true显示，false隐藏）', 0, NULL, NULL, 'sys.index.tagsView', '主框架页-是否开启页签', 'Y', 'true');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `ancestors` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `leader` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` bigint(20) NULL DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sup_dept` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK6bh7ihcbbqmtrhid0sybhrr4y`(`sup_dept`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 'admin', '2020-07-09 16:24:48', 0, NULL, 0, NULL, NULL, '0', '海纳软件', '123@qq.com', '曹', 0, '18888888888', '0', NULL);
INSERT INTO `sys_dept` VALUES (2, 'admin', '2022-01-17 09:29:23', 1, NULL, 0, NULL, NULL, '0,1', '开发部', '123@qq.com', 'XXX', 1, '15888888888', '0', 1);
INSERT INTO `sys_dept` VALUES (3, 'admin', '2022-01-17 09:29:51', 2, NULL, 0, NULL, NULL, '0,1', '运营部', '123@qq.com', 'XXX', 1, '15866666666', '0', 1);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_sort` int(11) NULL DEFAULT NULL,
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_default` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `list_class` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 'admin', '2020-07-09 16:25:27', NULL, '性别男', 0, NULL, NULL, NULL, '男', 1, 'sys_user_sex', '0', 'Y', '', '0');
INSERT INTO `sys_dict_data` VALUES (2, 'admin', '2020-07-09 16:25:27', NULL, '性别女', 0, NULL, NULL, NULL, '女', 2, 'sys_user_sex', '1', 'N', '', '0');
INSERT INTO `sys_dict_data` VALUES (3, 'admin', '2020-07-09 16:25:27', NULL, '性别未知', 0, NULL, NULL, NULL, '未知', 3, 'sys_user_sex', '2', 'N', '', '0');
INSERT INTO `sys_dict_data` VALUES (4, 'admin', '2020-07-09 16:25:27', NULL, '显示菜单', 0, NULL, NULL, NULL, '显示', 1, 'sys_show_hide', '0', 'Y', 'primary', '0');
INSERT INTO `sys_dict_data` VALUES (5, 'admin', '2020-07-09 16:25:27', NULL, '隐藏菜单', 0, NULL, NULL, NULL, '隐藏', 2, 'sys_show_hide', '1', 'N', 'danger', '0');
INSERT INTO `sys_dict_data` VALUES (6, 'admin', '2020-07-09 16:25:27', NULL, '正常状态', 0, NULL, NULL, NULL, '正常', 1, 'sys_normal_disable', '0', 'Y', 'primary', '0');
INSERT INTO `sys_dict_data` VALUES (7, 'admin', '2020-07-09 16:25:27', NULL, '停用状态', 0, NULL, NULL, NULL, '停用', 2, 'sys_normal_disable', '1', 'N', 'danger', '0');
INSERT INTO `sys_dict_data` VALUES (8, 'admin', '2020-07-09 16:25:27', NULL, '正常状态', 0, NULL, NULL, NULL, '正常', 1, 'sys_job_status', '0', 'Y', 'primary', '0');
INSERT INTO `sys_dict_data` VALUES (9, 'admin', '2020-07-09 16:25:27', NULL, '停用状态', 0, NULL, NULL, NULL, '暂停', 2, 'sys_job_status', '1', 'N', 'danger', '0');
INSERT INTO `sys_dict_data` VALUES (10, 'admin', '2020-07-09 16:25:27', NULL, '默认分组', 0, NULL, NULL, NULL, '默认', 1, 'sys_job_group', 'DEFAULT', 'Y', '', '0');
INSERT INTO `sys_dict_data` VALUES (11, 'admin', '2020-07-09 16:25:28', NULL, '系统分组', 0, NULL, NULL, NULL, '系统', 2, 'sys_job_group', 'SYSTEM', 'N', '', '0');
INSERT INTO `sys_dict_data` VALUES (12, 'admin', '2020-07-09 16:25:28', NULL, '系统默认是', 0, NULL, NULL, NULL, '是', 1, 'sys_yes_no', 'Y', 'Y', 'primary', '0');
INSERT INTO `sys_dict_data` VALUES (13, 'admin', '2020-07-09 16:25:28', NULL, '系统默认否', 0, NULL, NULL, NULL, '否', 2, 'sys_yes_no', 'N', 'N', 'danger', '0');
INSERT INTO `sys_dict_data` VALUES (14, 'admin', '2020-07-09 16:25:28', NULL, '通知', 0, NULL, NULL, NULL, '通知', 1, 'sys_notice_type', '1', 'Y', 'warning', '0');
INSERT INTO `sys_dict_data` VALUES (15, 'admin', '2020-07-09 16:25:28', NULL, '公告', 0, NULL, NULL, NULL, '公告', 2, 'sys_notice_type', '2', 'N', 'success', '0');
INSERT INTO `sys_dict_data` VALUES (16, 'admin', '2020-07-09 16:25:28', NULL, '正常状态', 0, NULL, NULL, NULL, '正常', 1, 'sys_notice_status', '0', 'Y', 'primary', '0');
INSERT INTO `sys_dict_data` VALUES (17, 'admin', '2020-07-09 16:25:28', NULL, '关闭状态', 0, NULL, NULL, NULL, '关闭', 2, 'sys_notice_status', '1', 'N', 'danger', '0');
INSERT INTO `sys_dict_data` VALUES (18, 'admin', '2020-07-09 16:25:28', NULL, '其他操作', 0, NULL, NULL, NULL, '其他', 99, 'sys_oper_type', '0', 'N', 'info', '0');
INSERT INTO `sys_dict_data` VALUES (19, 'admin', '2020-07-09 16:25:28', NULL, '新增操作', 0, NULL, NULL, NULL, '新增', 1, 'sys_oper_type', '1', 'N', 'info', '0');
INSERT INTO `sys_dict_data` VALUES (20, 'admin', '2020-07-09 16:25:28', NULL, '修改操作', 0, NULL, NULL, NULL, '修改', 2, 'sys_oper_type', '2', 'N', 'info', '0');
INSERT INTO `sys_dict_data` VALUES (21, 'admin', '2020-07-09 16:25:28', NULL, '删除操作', 0, NULL, NULL, NULL, '删除', 3, 'sys_oper_type', '3', 'N', 'danger', '0');
INSERT INTO `sys_dict_data` VALUES (22, 'admin', '2020-07-09 16:25:28', NULL, '授权操作', 0, NULL, NULL, NULL, '授权', 4, 'sys_oper_type', '4', 'N', 'primary', '0');
INSERT INTO `sys_dict_data` VALUES (23, 'admin', '2020-07-09 16:25:28', NULL, '导出操作', 0, NULL, NULL, NULL, '导出', 5, 'sys_oper_type', '5', 'N', 'warning', '0');
INSERT INTO `sys_dict_data` VALUES (24, 'admin', '2020-07-09 16:25:28', NULL, '导入操作', 0, NULL, NULL, NULL, '导入', 6, 'sys_oper_type', '6', 'N', 'warning', '0');
INSERT INTO `sys_dict_data` VALUES (25, 'admin', '2020-07-09 16:25:28', NULL, '强退操作', 0, NULL, NULL, NULL, '强退', 7, 'sys_oper_type', '7', 'N', 'danger', '0');
INSERT INTO `sys_dict_data` VALUES (26, 'admin', '2020-07-09 16:25:28', NULL, '清空数据', 0, NULL, NULL, NULL, '清空数据', 8, 'sys_oper_type', '8', 'N', 'danger', '0');
INSERT INTO `sys_dict_data` VALUES (27, 'admin', '2020-07-09 16:25:28', NULL, '正常状态', 0, NULL, NULL, NULL, '成功', 1, 'sys_common_status', '0', 'N', 'primary', '0');
INSERT INTO `sys_dict_data` VALUES (28, 'admin', '2020-07-09 16:25:28', NULL, '停用状态', 0, NULL, NULL, NULL, '失败', 2, 'sys_common_status', '1', 'N', 'danger', '0');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, 'admin', '2020-07-09 16:25:06', NULL, '用户性别列表', 0, NULL, NULL, '用户性别', 'sys_user_sex', '0');
INSERT INTO `sys_dict_type` VALUES (2, 'admin', '2020-07-09 16:25:06', NULL, '菜单状态列表', 0, NULL, NULL, '菜单状态', 'sys_show_hide', '0');
INSERT INTO `sys_dict_type` VALUES (3, 'admin', '2020-07-09 16:25:06', NULL, '系统开关列表', 0, NULL, NULL, '系统开关', 'sys_normal_disable', '0');
INSERT INTO `sys_dict_type` VALUES (4, 'admin', '2020-07-09 16:25:06', NULL, '任务状态列表', 0, NULL, NULL, '任务状态', 'sys_job_status', '0');
INSERT INTO `sys_dict_type` VALUES (5, 'admin', '2020-07-09 16:25:06', NULL, '任务分组列表', 0, NULL, NULL, '任务分组', 'sys_job_group', '0');
INSERT INTO `sys_dict_type` VALUES (6, 'admin', '2020-07-09 16:25:06', NULL, '系统是否列表', 0, NULL, NULL, '系统是否', 'sys_yes_no', '0');
INSERT INTO `sys_dict_type` VALUES (7, 'admin', '2020-07-09 16:25:06', NULL, '通知类型列表', 0, NULL, NULL, '通知类型', 'sys_notice_type', '0');
INSERT INTO `sys_dict_type` VALUES (8, 'admin', '2020-07-09 16:25:06', NULL, '通知状态列表', 0, NULL, NULL, '通知状态', 'sys_notice_status', '0');
INSERT INTO `sys_dict_type` VALUES (9, 'admin', '2020-07-09 16:25:06', NULL, '操作类型列表', 0, NULL, NULL, '操作类型', 'sys_oper_type', '0');
INSERT INTO `sys_dict_type` VALUES (10, 'admin', '2020-07-09 16:25:06', NULL, '系统状态列表', 0, NULL, NULL, '系统状态', 'sys_common_status', '0');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `concurrent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `invoke_target` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `misfire_policy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, 'admin', '2020-07-09 17:17:36', NULL, '', 0, NULL, '2020-07-09 17:17:36', '1', '* * * * * ?', 'mfTask.mfNoParams', 'DEFAULT', '系统默认（无参）', '3', '1');
INSERT INTO `sys_job` VALUES (2, 'admin', '2020-07-09 17:18:25', NULL, '', 0, NULL, '2020-07-09 17:18:25', '1', '* * * * * ?', 'mfTask.mfParams(\'mf\')', 'DEFAULT', '系统默认（有参）', '3', '1');
INSERT INTO `sys_job` VALUES (3, 'admin', '2020-07-09 17:19:38', NULL, '', 0, NULL, '2020-07-09 17:19:38', '1', '* * * * * ?', 'mfTask.mfMultipleParams(\'mf\', true, 2000L, 316.50D, 100)', 'DEFAULT', '系统默认（多参）', '3', '1');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NULL DEFAULT NULL,
  `exception_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `invoke_target` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ipaddr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_time` datetime NULL DEFAULT NULL,
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `os` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 78 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `menu_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` bigint(20) NULL DEFAULT NULL,
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `target` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `visible` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_refresh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 84 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 'admin', '2020-07-09 16:41:26', 1, '系统管理目录', 0, NULL, NULL, 'fa fa-gear', '系统管理', 'M', 0, '', '', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (2, 'admin', '2020-07-09 16:41:26', 2, '系统监控目录', 0, NULL, NULL, 'fa fa-video-camera', '系统监控', 'M', 0, '', NULL, '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (3, 'admin', '2020-07-09 16:41:26', 1, '菜单管理菜单', 0, NULL, NULL, '#', '菜单管理', 'C', 1, 'system:menu:view', '', '/system/menu', '0', NULL);
INSERT INTO `sys_menu` VALUES (4, 'admin', '2020-07-09 16:42:30', 2, NULL, 0, NULL, '2020-07-09 16:42:30', '#', '用户管理', 'C', 1, 'system:user:view', 'menuItem', '/system/user', '0', NULL);
INSERT INTO `sys_menu` VALUES (5, 'admin', '2020-07-09 16:43:00', 3, NULL, 0, NULL, '2020-07-09 16:43:00', '#', '角色管理', 'C', 1, 'system:role:view', 'menuItem', '/system/role', '0', NULL);
INSERT INTO `sys_menu` VALUES (6, 'admin', '2020-07-09 16:43:33', 4, NULL, 0, NULL, '2020-07-09 16:43:33', '#', '部门管理', 'C', 1, 'system:dept:view', 'menuItem', '/system/dept', '0', NULL);
INSERT INTO `sys_menu` VALUES (7, 'admin', '2020-07-09 16:44:03', 5, NULL, 0, NULL, '2020-07-09 16:44:03', '#', '岗位管理', 'C', 1, 'system:post:view', 'menuItem', '/system/post', '0', NULL);
INSERT INTO `sys_menu` VALUES (8, 'admin', '2020-07-09 16:44:35', 6, NULL, 0, NULL, '2020-07-09 16:44:35', '#', '字典管理', 'C', 1, 'system:dict:view', 'menuItem', '/system/dict', '0', NULL);
INSERT INTO `sys_menu` VALUES (9, 'admin', '2020-07-09 16:45:08', 7, NULL, 0, NULL, '2020-07-09 16:45:08', '#', '参数设置', 'C', 1, 'system:config:view', 'menuItem', '/system/config', '0', NULL);
INSERT INTO `sys_menu` VALUES (10, 'admin', '2020-07-09 16:45:38', 8, NULL, 0, NULL, '2020-07-09 16:45:38', '#', '通知公告', 'C', 1, 'system:notice:view', 'menuItem', '/system/notice', '0', NULL);
INSERT INTO `sys_menu` VALUES (11, 'admin', '2020-07-09 16:46:06', 9, NULL, 0, NULL, '2020-07-09 16:46:06', '#', '日志管理', 'M', 1, '', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (12, 'admin', '2020-07-09 16:46:48', 1, NULL, 0, NULL, '2020-07-09 16:46:48', '#', '在线用户', 'C', 2, 'monitor:online:view', 'menuItem', '/monitor/online', '0', NULL);
INSERT INTO `sys_menu` VALUES (13, 'admin', '2020-07-09 16:47:20', 2, NULL, 0, NULL, '2020-07-09 16:47:20', '#', '定时任务', 'C', 2, 'monitor:job:view', 'menuItem', '/monitor/job', '0', NULL);
INSERT INTO `sys_menu` VALUES (14, 'admin', '2020-07-09 16:47:52', 3, NULL, 0, NULL, '2020-07-09 16:47:52', '#', '数据监控', 'C', 2, 'monitor:data:view', 'menuItem', '/monitor/data', '0', NULL);
INSERT INTO `sys_menu` VALUES (15, 'admin', '2020-07-09 16:48:23', 4, NULL, 0, NULL, '2020-07-09 16:48:23', '#', '服务监控', 'C', 2, 'monitor:server:view', 'menuItem', '/monitor/server', '0', NULL);
INSERT INTO `sys_menu` VALUES (16, 'admin', '2020-07-09 16:49:36', 1, NULL, 0, NULL, '2020-07-09 16:49:36', '', '菜单查询', 'F', 3, 'system:menu:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (17, 'admin', '2020-07-09 16:49:57', 2, NULL, 0, NULL, '2020-07-09 16:49:57', '', '菜单新增', 'F', 3, 'system:menu:add', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (18, 'admin', '2020-07-09 16:50:19', 3, NULL, 0, NULL, '2020-07-09 16:50:19', '', '菜单修改', 'F', 3, 'system:menu:edit', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (19, 'admin', '2020-07-09 16:50:39', 4, NULL, 0, NULL, '2020-07-09 16:50:39', '', '菜单删除', 'F', 3, 'system:menu:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (20, 'admin', '2020-07-09 16:51:01', 1, NULL, 0, NULL, '2020-07-09 16:51:01', '', '用户查询', 'F', 4, 'system:user:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (21, 'admin', '2020-07-09 16:51:18', 2, NULL, 0, NULL, '2020-07-09 16:51:18', '', '用户新增', 'F', 4, 'system:user:add', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (22, 'admin', '2020-07-09 16:51:39', 3, NULL, 0, NULL, '2020-07-09 16:51:39', '', '用户修改', 'F', 4, 'system:user:edit', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (23, 'admin', '2020-07-09 16:51:57', 4, NULL, 0, NULL, '2020-07-09 16:51:57', '', '用户删除', 'F', 4, 'system:user:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (24, 'admin', '2020-07-09 16:52:17', 5, NULL, 0, NULL, '2020-07-09 16:52:17', '', '用户导出', 'F', 4, 'system:user:export', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (25, 'admin', '2020-07-09 16:52:47', 6, NULL, 0, NULL, '2020-07-09 16:52:47', '', '用户导入', 'F', 4, 'system:user:import', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (26, 'admin', '2020-07-09 16:53:07', 7, NULL, 0, NULL, '2020-07-09 16:53:07', '', '重置密码', 'F', 4, 'system:user:resetPwd', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (27, 'admin', '2020-07-09 16:53:41', 1, NULL, 0, NULL, '2020-07-09 16:53:41', '', '角色查询', 'F', 5, 'system:role:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (28, 'admin', '2020-07-09 16:53:59', 2, NULL, 0, NULL, '2020-07-09 16:53:59', '', '角色新增', 'F', 5, 'system:role:add', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (29, 'admin', '2020-07-09 16:54:18', 3, NULL, 0, NULL, '2020-07-09 16:54:18', '', '角色修改', 'F', 5, 'system:role:edit', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (30, 'admin', '2020-07-09 16:54:42', 4, NULL, 0, NULL, '2020-07-09 16:54:42', '', '角色删除', 'F', 5, 'system:role:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (31, 'admin', '2020-07-09 16:55:01', 5, NULL, 0, NULL, '2020-07-09 16:55:01', '', '角色导出', 'F', 5, 'system:role:export', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (32, 'admin', '2020-07-09 16:55:30', 1, NULL, 0, NULL, '2020-07-09 16:55:30', '', '部门查询', 'F', 6, 'system:dept:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (33, 'admin', '2020-07-09 16:55:52', 2, NULL, 0, NULL, '2020-07-09 16:55:52', '', '部门新增', 'F', 6, 'system:dept:add', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (34, 'admin', '2020-07-09 16:56:18', 3, NULL, 0, NULL, '2020-07-09 16:56:18', '', '部门修改', 'F', 6, 'system:dept:edit', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (35, 'admin', '2020-07-09 16:56:40', 4, NULL, 0, NULL, '2020-07-09 16:56:40', '', '部门删除', 'F', 6, 'system:dept:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (36, 'admin', '2020-07-09 16:57:10', 1, NULL, 0, NULL, '2020-07-09 16:57:10', '', '岗位查询', 'F', 7, 'system:post:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (37, 'admin', '2020-07-09 16:57:34', 2, NULL, 0, NULL, '2020-07-09 16:57:34', '', '岗位新增', 'F', 7, 'system:post:add', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (38, 'admin', '2020-07-09 16:57:55', 3, NULL, 0, NULL, '2020-07-09 16:57:55', '', '岗位修改', 'F', 7, 'system:post:edit', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (39, 'admin', '2020-07-09 16:58:13', 4, NULL, 0, NULL, '2020-07-09 16:58:13', '', '岗位删除', 'F', 7, 'system:post:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (40, 'admin', '2020-07-09 16:58:33', 5, NULL, 0, NULL, '2020-07-09 16:58:33', '', '岗位导出', 'F', 7, 'system:post:export', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (41, 'admin', '2020-07-09 16:59:07', 1, NULL, 0, NULL, '2020-07-09 16:59:07', '', '字典查询', 'F', 8, 'system:dict:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (42, 'admin', '2020-07-09 16:59:25', 2, NULL, 0, NULL, '2020-07-09 16:59:25', '', '字典新增', 'F', 8, 'system:dict:add', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (43, 'admin', '2020-07-09 16:59:46', 3, NULL, 0, NULL, '2020-07-09 16:59:46', '', '字典修改', 'F', 8, 'system:dict:edit', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (44, 'admin', '2020-07-09 17:00:09', 4, NULL, 0, NULL, '2020-07-09 17:00:09', '', '字典删除', 'F', 8, 'system:dict:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (45, 'admin', '2020-07-09 17:00:30', 5, NULL, 0, NULL, '2020-07-09 17:00:30', '', '字典导出', 'F', 8, 'system:dict:export', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (46, 'admin', '2020-07-09 17:01:10', 1, NULL, 0, NULL, '2020-07-09 17:01:10', '', '参数查询', 'F', 9, 'system:config:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (47, 'admin', '2020-07-09 17:01:38', 2, NULL, 0, NULL, '2020-07-09 17:01:38', '', '参数新增', 'F', 9, 'system:config:add', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (48, 'admin', '2020-07-09 17:02:06', 3, NULL, 0, NULL, '2020-07-09 17:02:06', '', '参数修改', 'F', 9, 'system:config:edit', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (49, 'admin', '2020-07-09 17:02:25', 4, NULL, 0, NULL, '2020-07-09 17:02:25', '', '参数删除', 'F', 9, 'system:config:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (50, 'admin', '2020-07-09 17:02:44', 5, NULL, 0, NULL, '2020-07-09 17:02:44', '', '参数导出', 'F', 9, 'system:config:export', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (51, 'admin', '2020-07-09 17:03:16', 1, NULL, 0, NULL, '2020-07-09 17:03:16', '', '公告查询', 'F', 10, 'system:notice:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (52, 'admin', '2020-07-09 17:03:37', 2, NULL, 0, NULL, '2020-07-09 17:03:37', '', '公告新增', 'F', 10, 'system:notice:add', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (53, 'admin', '2020-07-09 17:03:55', 3, NULL, 0, NULL, '2020-07-09 17:03:55', '', '公告修改', 'F', 10, 'system:notice:edit', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (54, 'admin', '2020-07-09 17:04:14', 4, NULL, 0, NULL, '2020-07-09 17:04:14', '', '公告删除', 'F', 10, 'system:notice:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (55, 'admin', '2020-07-09 17:05:00', 1, NULL, 0, NULL, '2020-07-09 17:05:00', '#', '登录日志', 'C', 11, 'monitor:logininfor:view', 'menuItem', '/monitor/logininfor', '0', NULL);
INSERT INTO `sys_menu` VALUES (56, 'admin', '2020-07-09 17:05:30', 1, NULL, 0, NULL, '2020-07-09 17:05:30', '', '登录查询', 'F', 55, 'monitor:logininfor:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (57, 'admin', '2020-07-09 17:05:48', 2, NULL, 0, NULL, '2020-07-09 17:05:48', '', '登录删除', 'F', 55, 'monitor:logininfor:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (58, 'admin', '2020-07-09 17:06:10', 3, NULL, 0, NULL, '2020-07-09 17:06:10', '', '日志导出', 'F', 55, 'monitor:logininfor:export', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (59, 'admin', '2020-07-09 17:06:47', 4, NULL, 0, NULL, '2020-07-09 17:06:47', '', '账户解锁', 'F', 55, 'monitor:logininfor:unlock', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (60, 'admin', '2020-07-09 17:07:14', 2, NULL, 0, NULL, '2020-07-09 17:07:14', '#', '操作日志', 'C', 11, 'monitor:operlog:view', 'menuItem', '/monitor/operlog', '0', NULL);
INSERT INTO `sys_menu` VALUES (61, 'admin', '2020-07-09 17:07:39', 1, NULL, 0, NULL, '2020-07-09 17:07:39', '', '操作查询', 'F', 60, 'monitor:operlog:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (62, 'admin', '2020-07-09 17:08:02', 2, NULL, 0, NULL, '2020-07-09 17:08:02', '', '操作删除', 'F', 60, 'monitor:operlog:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (63, 'admin', '2020-07-09 17:08:26', 3, NULL, 0, NULL, '2020-07-09 17:08:26', '', '详细信息', 'F', 60, 'monitor:operlog:detail', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (64, 'admin', '2020-07-09 17:08:49', 4, NULL, 0, NULL, '2020-07-09 17:08:49', '', '日志导出', 'F', 60, 'monitor:operlog:export', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (65, 'admin', '2020-07-09 17:09:30', 1, NULL, 0, NULL, '2020-07-09 17:09:30', '', '在线查询', 'F', 12, 'monitor:online:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (66, 'admin', '2020-07-09 17:09:56', 2, NULL, 0, NULL, '2020-07-09 17:09:56', '', '批量强退', 'F', 12, 'monitor:online:batchForceLogout', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (67, 'admin', '2020-07-09 17:10:18', 3, NULL, 0, NULL, '2020-07-09 17:10:18', '', '单条强退', 'F', 12, 'monitor:online:forceLogout', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (68, 'admin', '2020-07-09 17:10:48', 1, NULL, 0, NULL, '2020-07-09 17:10:48', '', '任务查询', 'F', 13, 'monitor:job:list', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (69, 'admin', '2020-07-09 17:11:11', 2, NULL, 0, NULL, '2020-07-09 17:11:11', '', '任务新增', 'F', 13, 'monitor:job:add', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (70, 'admin', '2020-07-09 17:11:31', 3, NULL, 0, NULL, '2020-07-09 17:11:31', '', '任务修改', 'F', 13, 'monitor:job:edit', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (71, 'admin', '2020-07-09 17:12:04', 4, NULL, 0, NULL, '2020-07-09 17:12:04', '', '任务删除', 'F', 13, 'monitor:job:remove', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (72, 'admin', '2020-07-09 17:12:30', 5, NULL, 0, NULL, '2020-07-09 17:12:30', '', '状态修改', 'F', 13, 'monitor:job:changeStatus', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (73, 'admin', '2020-07-09 17:12:50', 6, NULL, 0, NULL, '2020-07-09 17:12:50', '', '任务详细', 'F', 13, 'monitor:job:detail', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (74, 'admin', '2020-07-09 17:13:12', 7, NULL, 0, NULL, '2020-07-09 17:13:12', '', '任务导出', 'F', 13, 'monitor:job:export', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (75, 'admin', '2020-09-07 08:23:38', 3, NULL, 0, NULL, '2020-09-07 08:23:38', 'fa fa-navicon', '系统工具', 'M', 0, '', 'menuItem', '#', '0', NULL);
INSERT INTO `sys_menu` VALUES (82, 'admin', '2021-06-21 16:16:50', 2, NULL, 0, NULL, '2021-06-21 16:16:50', 'fa fa-gg', '系统接口', 'C', 75, 'tool:swagger:view', 'menuBlank', '/tool/swagger', '0', NULL);
INSERT INTO `sys_menu` VALUES (83, 'admin', '2021-06-21 16:18:07', 5, NULL, 0, NULL, '2021-06-21 16:18:07', 'fa fa-cube', '缓存监控', 'C', 2, 'monitor:cache:view', 'menuItem', '/monitor/cache', '0', NULL);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `notice_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notice_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `business_type` int(11) NULL DEFAULT NULL,
  `dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `error_msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `json_result` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `oper_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `oper_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `oper_param` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `oper_time` datetime NULL DEFAULT NULL,
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `operator_type` int(11) NULL DEFAULT NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `request_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 47 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `flag` bit(1) NOT NULL,
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `data_scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `flag` bit(1) NOT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'admin', '2020-07-09 17:13:59', NULL, b'0', 1, '超级管理员', 0, 'admin', '超级管理员', '0', NULL, '2020-07-09 17:13:59');
INSERT INTO `sys_role` VALUES (2, 'admin', '2022-01-17 10:19:53', '3', b'0', 2, '2', 0, 'common', '测试', '0', NULL, '2022-01-17 10:19:53');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dept_id` bigint(20) NULL DEFAULT NULL,
  `role_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKp8ajq80m63s361m1pq3isls5t`(`dept_id`) USING BTREE,
  INDEX `FKmdoybh4v5t2ooi48m3307n7fx`(`role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `menu_id` bigint(20) NULL DEFAULT NULL,
  `role_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKf3mud4qoc7ayew8nml4plkevo`(`menu_id`) USING BTREE,
  INDEX `FKkeitxsgxwayackgqllio4ohn5`(`role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 55 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1, 2);
INSERT INTO `sys_role_menu` VALUES (2, 4, 2);
INSERT INTO `sys_role_menu` VALUES (3, 20, 2);
INSERT INTO `sys_role_menu` VALUES (4, 21, 2);
INSERT INTO `sys_role_menu` VALUES (5, 22, 2);
INSERT INTO `sys_role_menu` VALUES (6, 23, 2);
INSERT INTO `sys_role_menu` VALUES (7, 24, 2);
INSERT INTO `sys_role_menu` VALUES (8, 25, 2);
INSERT INTO `sys_role_menu` VALUES (9, 26, 2);
INSERT INTO `sys_role_menu` VALUES (10, 5, 2);
INSERT INTO `sys_role_menu` VALUES (11, 27, 2);
INSERT INTO `sys_role_menu` VALUES (12, 28, 2);
INSERT INTO `sys_role_menu` VALUES (13, 29, 2);
INSERT INTO `sys_role_menu` VALUES (14, 30, 2);
INSERT INTO `sys_role_menu` VALUES (15, 31, 2);
INSERT INTO `sys_role_menu` VALUES (16, 6, 2);
INSERT INTO `sys_role_menu` VALUES (17, 32, 2);
INSERT INTO `sys_role_menu` VALUES (18, 33, 2);
INSERT INTO `sys_role_menu` VALUES (19, 34, 2);
INSERT INTO `sys_role_menu` VALUES (20, 35, 2);
INSERT INTO `sys_role_menu` VALUES (21, 10, 2);
INSERT INTO `sys_role_menu` VALUES (22, 51, 2);
INSERT INTO `sys_role_menu` VALUES (23, 52, 2);
INSERT INTO `sys_role_menu` VALUES (24, 53, 2);
INSERT INTO `sys_role_menu` VALUES (25, 54, 2);
INSERT INTO `sys_role_menu` VALUES (26, 11, 2);
INSERT INTO `sys_role_menu` VALUES (27, 55, 2);
INSERT INTO `sys_role_menu` VALUES (28, 56, 2);
INSERT INTO `sys_role_menu` VALUES (29, 57, 2);
INSERT INTO `sys_role_menu` VALUES (30, 58, 2);
INSERT INTO `sys_role_menu` VALUES (31, 59, 2);
INSERT INTO `sys_role_menu` VALUES (32, 60, 2);
INSERT INTO `sys_role_menu` VALUES (33, 61, 2);
INSERT INTO `sys_role_menu` VALUES (34, 62, 2);
INSERT INTO `sys_role_menu` VALUES (35, 63, 2);
INSERT INTO `sys_role_menu` VALUES (36, 64, 2);
INSERT INTO `sys_role_menu` VALUES (37, 2, 2);
INSERT INTO `sys_role_menu` VALUES (38, 12, 2);
INSERT INTO `sys_role_menu` VALUES (39, 65, 2);
INSERT INTO `sys_role_menu` VALUES (40, 66, 2);
INSERT INTO `sys_role_menu` VALUES (41, 67, 2);
INSERT INTO `sys_role_menu` VALUES (42, 13, 2);
INSERT INTO `sys_role_menu` VALUES (43, 68, 2);
INSERT INTO `sys_role_menu` VALUES (44, 69, 2);
INSERT INTO `sys_role_menu` VALUES (45, 70, 2);
INSERT INTO `sys_role_menu` VALUES (46, 71, 2);
INSERT INTO `sys_role_menu` VALUES (47, 72, 2);
INSERT INTO `sys_role_menu` VALUES (48, 73, 2);
INSERT INTO `sys_role_menu` VALUES (49, 74, 2);
INSERT INTO `sys_role_menu` VALUES (50, 14, 2);
INSERT INTO `sys_role_menu` VALUES (51, 15, 2);
INSERT INTO `sys_role_menu` VALUES (52, 83, 2);
INSERT INTO `sys_role_menu` VALUES (53, 75, 2);
INSERT INTO `sys_role_menu` VALUES (54, 82, 2);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `dept_id` bigint(20) NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_date` datetime NULL DEFAULT NULL,
  `login_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order_num` int(11) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remove_status` int(11) NULL DEFAULT NULL,
  `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dept` bigint(20) NULL DEFAULT NULL,
  `pwd_update_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKes6oly9oxgahljuwv87fgl44p`(`dept`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, '', 'admin', '2020-07-09 16:25:54', NULL, '123@qq.com', '2022-06-11 09:38:09', '127.0.0.1', 'admin', NULL, '2d01c00fdaa1a6c7f7665058b28c8f22', '18888888888', '超级管理员', 0, '75b743', '1', '0', NULL, NULL, '超管', NULL, NULL);
INSERT INTO `sys_user` VALUES (2, '', 'admin', '2022-01-17 10:14:31', 2, '456@qq.com', '2022-01-19 08:52:35', '127.0.0.1', 'yd', NULL, '896318d60dc5ef62515ce4bb8f469bd0', '15666666666', '测试', 0, 'ebc93e', '0', '0', NULL, NULL, '测试', 2, NULL);
INSERT INTO `sys_user` VALUES (3, '', 'admin', '2022-01-17 15:34:40', 2, '555@qq.com', '2022-01-19 09:29:39', '127.0.0.1', 'test', NULL, '9890432984539957cf0b78bdd97c559f', '15888886666', '', 0, 'cd26b5', '0', '0', 'admin', '2022-01-19 09:27:18', 'test', 2, NULL);

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expire_time` bigint(20) NULL DEFAULT NULL,
  `ipaddr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_access_time` datetime NULL DEFAULT NULL,
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `os` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `session_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start_time_stamp` datetime NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 47 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NULL DEFAULT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKng2mc7xcmyerevvobtw95bmu9`(`post_id`) USING BTREE,
  INDEX `FKpjx0gi8xwm66cp1w1jvi4hc57`(`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKhh52n8vd4ny9ff4x9fb8v65qx`(`role_id`) USING BTREE,
  INDEX `FKb40xxfch70f5qnyfw8yme1n1s`(`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 1);
INSERT INTO `sys_user_role` VALUES (11, 2, 3);
INSERT INTO `sys_user_role` VALUES (10, 2, 2);

SET FOREIGN_KEY_CHECKS = 1;
